package tabacco

import (
	"context"
	"time"

	"github.com/prometheus/client_golang/prometheus"

	"git.autistici.org/ai3/tools/tabacco/jobs"
)

var (
	backupSuccesses = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "backup_ok",
			Help: "Did the backup succeed.",
		},
		[]string{"dataset"},
	)
	backupElapsed = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "backup_elapsed",
			Help: "Time taken by the last backup.",
		},
		[]string{"dataset"},
	)
	backupLastRun = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "backup_last_run_timestamp",
			Help: "Timestamp of the last backup (succeeded or not).",
		},
		[]string{"dataset"},
	)
	backupLastOk = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "backup_last_ok_timestamp",
			Help: "Timestamp of the last successful backup.",
		},
		[]string{"dataset"},
	)
)

func init() {
	prometheus.MustRegister(backupSuccesses, backupElapsed, backupLastRun, backupLastOk)
}

// Wrap a job with prometheus success/lastRun instrumentation based on
// the exit status of the job.
func withInstrumentation(j jobs.Job, dataset string) jobs.Job {
	return jobs.JobFunc(func(ctx context.Context) error {
		start := time.Now()

		err := j.RunContext(ctx)

		l := prometheus.Labels{"dataset": dataset}
		elapsed := time.Since(start)
		backupElapsed.With(l).Set(elapsed.Seconds())
		backupLastRun.With(l).Set(float64(start.Unix()))
		var statusValue float64
		if err == nil {
			backupLastOk.With(l).Set(float64(start.Unix()))
			statusValue = 1
		}
		backupSuccesses.With(l).Set(statusValue)

		return err
	})
}
