package server

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"math/rand"
	"time"

	"git.autistici.org/ai3/go-common/sqlutil"
	"github.com/mattn/go-sqlite3"
)

var migrations = []func(*sql.Tx) error{
	sqlutil.Statement(`
--- Denormalized log table.
CREATE TABLE log (
        backup_id VARCHAR(128),
        backup_timestamp DATETIME,
        backup_host VARCHAR(128),
        dataset_id VARCHAR(128),
        dataset_source VARCHAR(128),
        atom_name VARCHAR(255),
        atom_full_path VARCHAR(255),
        atom_path VARCHAR(255)
);
`, `
CREATE UNIQUE INDEX idx_log_primary ON log (backup_id, dataset_id, atom_name);
`, `
CREATE INDEX idx_log_backup_id ON log (backup_id);
`, `
CREATE INDEX idx_log_backup_id_and_dataset_id ON log (backup_id, dataset_id);
`),
	sqlutil.Statement(`
ALTER TABLE log ADD COLUMN dataset_snapshot_id VARCHAR(64);
`),
	sqlutil.Statement(`
ALTER TABLE log ADD COLUMN dataset_total_files INTEGER DEFAULT 0;
`, `
ALTER TABLE log ADD COLUMN dataset_total_bytes INTEGER DEFAULT 0;
`, `
ALTER TABLE log ADD COLUMN dataset_bytes_added INTEGER DEFAULT 0;
`, `
ALTER TABLE log ADD COLUMN dataset_duration INTEGER DEFAULT 0;
`),
	sqlutil.Statement(`
CREATE INDEX idx_log_backup_timestamp ON log (backup_timestamp);
`, `
CREATE INDEX idx_log_dataset_source ON log (dataset_source);
`),
	sqlutil.Statement(`
CREATE INDEX idx_log_backup_host ON log (backup_host);
`, `
CREATE INDEX idx_log_dataset_id ON log (dataset_id);
`),
	// Speeds up the get_sources_size query.
	sqlutil.Statement(`
CREATE INDEX idx_log_hst ON log (backup_host, dataset_source, backup_timestamp);
`),
}

func openDB(dbDriver, dbURI string) (*sql.DB, error) {
	if dbDriver != "sqlite3" {
		return nil, fmt.Errorf("only the sqlite3 sql driver is supported")
	}
	return sqlutil.OpenDB(dbURI, sqlutil.WithMigrations(migrations))
}

func isBusy(err error) bool {
	switch e := err.(type) {
	case sqlite3.Error:
		return e.Code == 5
	default:
		return false
	}
}

var defaultQueryTimeout = 3 * time.Second

func retryBusy(ctx context.Context, f func() error) error {
	deadline, ok := ctx.Deadline()
	if !ok {
		deadline = time.Now().Add(defaultQueryTimeout)
	}

	for time.Now().Before(deadline) {
		if err := f(); !isBusy(err) {
			return err
		}

		// Random sleep, max 1ms.
		time.Sleep(time.Duration(rand.Float64()) * time.Millisecond)
	}
	return errors.New("query timed out waiting to lock the database")
}
