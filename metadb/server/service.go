package server

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"path/filepath"
	"strings"
	"time"

	"git.autistici.org/ai3/go-common/sqlutil"
	"git.autistici.org/ai3/tools/tabacco"
)

// An atom, as represented in the database, denormalized.
type dbAtom struct {
	BackupID          string
	BackupTimestamp   time.Time
	BackupHost        string
	DatasetID         string
	DatasetSource     string
	DatasetSnapshotID string
	DatasetTotalFiles int64
	DatasetTotalBytes int64
	DatasetBytesAdded int64
	DatasetDuration   int
	AtomName          string
	AtomFullPath      string
	AtomPath          string
}

func makeAtoms(backup tabacco.Backup, ds tabacco.Dataset) []dbAtom {
	var out []dbAtom
	for _, atom := range ds.Atoms {

		// It is here that we 'materialize' the concept of Atom names
		// as paths, by concatenating source/dataset/atom and storing
		// it as the atom name.
		path := filepath.Join(ds.Source, atom.Name)

		out = append(out, dbAtom{
			BackupID:          backup.ID,
			BackupTimestamp:   backup.Timestamp,
			BackupHost:        backup.Host,
			DatasetID:         ds.ID,
			DatasetSnapshotID: ds.SnapshotID,
			DatasetSource:     ds.Source,
			DatasetTotalFiles: ds.TotalFiles,
			DatasetTotalBytes: ds.TotalBytes,
			DatasetBytesAdded: ds.BytesAdded,
			DatasetDuration:   ds.Duration,
			AtomName:          atom.Name,
			AtomPath:          atom.Path,
			AtomFullPath:      path,
		})
	}
	return out
}

func (a *dbAtom) getBackup() *tabacco.Backup {
	return &tabacco.Backup{
		ID:        a.BackupID,
		Timestamp: a.BackupTimestamp,
		Host:      a.BackupHost,
	}
}

func (a *dbAtom) getDataset() *tabacco.Dataset {
	return &tabacco.Dataset{
		ID:         a.DatasetID,
		SnapshotID: a.DatasetSnapshotID,
		Source:     a.DatasetSource,
		TotalFiles: a.DatasetTotalFiles,
		TotalBytes: a.DatasetTotalBytes,
		BytesAdded: a.DatasetBytesAdded,
		Duration:   a.DatasetDuration,
	}
}

func (a *dbAtom) getAtom() tabacco.Atom {
	return tabacco.Atom{
		Name: a.AtomName,
		Path: a.AtomPath,
	}
}

func keepNumVersions(dbAtoms []*dbAtom, numVersions int) []*dbAtom {
	// numVersions == 0 is remapped to 1.
	if numVersions < 1 {
		numVersions = 1
	}

	count := 0
	tmp := make(map[string][]*dbAtom)
	for _, a := range dbAtoms {
		l := tmp[a.AtomFullPath]
		if len(l) < numVersions {
			l = append(l, a)
			count++
		}
		tmp[a.AtomFullPath] = l
	}
	out := make([]*dbAtom, 0, count)
	for _, l := range tmp {
		out = append(out, l...)
	}
	return out
}

func groupByBackup(dbAtoms []*dbAtom) []*tabacco.Backup {
	// As we scan through dbAtoms, aggregate into Backups and Datasets.
	backups := make(map[string]*tabacco.Backup)
	dsm := make(map[string]map[string]*tabacco.Dataset)

	for _, atom := range dbAtoms {
		// Create the Backup object if it does not exist.
		b, ok := backups[atom.BackupID]
		if !ok {
			b = atom.getBackup()
			backups[atom.BackupID] = b
		}

		// Create the Dataset object for this Backup in the
		// two-level map (creating the intermediate map if
		// necessary).
		tmp, ok := dsm[atom.BackupID]
		if !ok {
			tmp = make(map[string]*tabacco.Dataset)
			dsm[atom.BackupID] = tmp
		}
		ds, ok := tmp[atom.DatasetID]
		if !ok {
			ds = atom.getDataset()
			tmp[atom.DatasetID] = ds
			b.Datasets = append(b.Datasets, ds)
		}

		// Finally, add the atom to the dataset.
		ds.Atoms = append(ds.Atoms, atom.getAtom())
	}

	out := make([]*tabacco.Backup, 0, len(backups))
	for _, b := range backups {
		out = append(out, b)
	}
	return out
}

// Service implementation of the tabacco backup metadata server API.
type Service struct {
	db     *sql.DB
	maxAge int
	stop   chan struct{}
}

// New creates a new service and returns it.
func New(dbDriver, dbURI string, maxMetadataAgeDays int) (*Service, error) {
	db, err := openDB(dbDriver, dbURI)
	if err != nil {
		return nil, err
	}

	s := &Service{
		db:     db,
		maxAge: maxMetadataAgeDays,
		stop:   make(chan struct{}),
	}
	if s.maxAge > 0 {
		go func() {
			t := time.NewTicker(12 * time.Hour)
			defer t.Stop()
			for {
				select {
				case <-t.C:
					if err := s.purge(); err != nil {
						log.Printf("purge() error: %v", err)
					}
				case <-s.stop:
					return
				}
			}
		}()
	}

	return s, nil
}

// Close the underlying database and all associated resources.
func (s *Service) Close() {
	close(s.stop)
	s.db.Close() // nolint
}

var statements = map[string]string{
	"insert_atom": `
		INSERT INTO log (
		  backup_id, backup_timestamp, backup_host,
		  dataset_id, dataset_snapshot_id, dataset_source,
		  dataset_total_files, dataset_total_bytes, dataset_bytes_added,
		  dataset_duration,
		  atom_name, atom_path, atom_full_path
		) VALUES (
		  ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?
		)
`,
	"get_latest_atoms": `
		SELECT
		  backup_id, backup_timestamp, backup_host,
		  dataset_id, dataset_snapshot_id, dataset_source,
		  dataset_total_files, dataset_total_bytes, dataset_bytes_added,
		  dataset_duration,
		  atom_name, atom_path, atom_full_path
		FROM log
                ORDER BY backup_timestamp DESC
                LIMIT ?
`,
	"get_latest_datasets": `
		SELECT
		  backup_id, backup_timestamp, backup_host,
		  dataset_id, dataset_snapshot_id, dataset_source,
		  dataset_total_files, dataset_total_bytes, dataset_bytes_added,
		  dataset_duration, COUNT(*) AS num_atoms
		FROM log
		WHERE backup_id IN (
			SELECT backup_id
			FROM log
			GROUP BY backup_id, backup_timestamp
			ORDER BY backup_timestamp DESC
			LIMIT ?)
                GROUP BY
		  backup_id, backup_timestamp, backup_host,
		  dataset_id, dataset_snapshot_id, dataset_source,
		  dataset_total_files, dataset_total_bytes, dataset_bytes_added,
		  dataset_duration
		ORDER BY backup_timestamp DESC
`,
	"get_latest_datasets_by_source": `
		SELECT
		  backup_id, backup_timestamp, backup_host,
		  dataset_id, dataset_snapshot_id, dataset_source,
		  dataset_total_files, dataset_total_bytes, dataset_bytes_added,
		  dataset_duration, COUNT(*) AS num_atoms
		FROM log
		WHERE dataset_source = ?
                GROUP BY
		  backup_id, backup_timestamp, backup_host,
		  dataset_id, dataset_snapshot_id, dataset_source,
		  dataset_total_files, dataset_total_bytes, dataset_bytes_added,
		  dataset_duration
		ORDER BY backup_timestamp DESC
		LIMIT ?
`,
	"get_latest_datasets_by_host": `
		SELECT
		  backup_id, backup_timestamp, backup_host,
		  dataset_id, dataset_snapshot_id, dataset_source,
		  dataset_total_files, dataset_total_bytes, dataset_bytes_added,
		  dataset_duration, COUNT(*) AS num_atoms
		FROM log
		WHERE backup_host = ?
                GROUP BY
		  backup_id, backup_timestamp, backup_host,
		  dataset_id, dataset_snapshot_id, dataset_source,
		  dataset_total_files, dataset_total_bytes, dataset_bytes_added,
		  dataset_duration
		ORDER BY backup_timestamp DESC
		LIMIT ?
`,
	"get_backup_by_id": `
		SELECT
		  backup_id, backup_timestamp, backup_host,
		  dataset_id, dataset_snapshot_id, dataset_source,
		  dataset_total_files, dataset_total_bytes, dataset_bytes_added,
		  dataset_duration, COUNT(*) AS num_atoms
		FROM log
		WHERE backup_id = ?
                GROUP BY
		  backup_id, backup_timestamp, backup_host,
		  dataset_id, dataset_snapshot_id, dataset_source,
		  dataset_total_files, dataset_total_bytes, dataset_bytes_added,
		  dataset_duration
		ORDER BY dataset_source ASC
`,
	"get_dataset_by_id": `
		SELECT
		  backup_id, backup_timestamp, backup_host,
		  dataset_id, dataset_snapshot_id, dataset_source,
		  dataset_total_files, dataset_total_bytes, dataset_bytes_added,
		  dataset_duration,
		  atom_name, atom_path, atom_full_path
		FROM log
		WHERE dataset_id = ?
		ORDER BY atom_name ASC
`,
	"get_latest_backups_by_source": `
                SELECT
                  backup_id, backup_timestamp, backup_host, dataset_source
                FROM log
                GROUP BY dataset_source, backup_host
                HAVING backup_timestamp = max(backup_timestamp)
                ORDER BY dataset_source ASC
`,
	"get_sources_size": `
                SELECT d.dataset_source, sum(d.total_bytes) FROM (
                  SELECT
                    a.dataset_source, max(a.dataset_total_bytes) AS total_bytes
                  FROM log AS a JOIN (
                    SELECT backup_id FROM log
                    GROUP BY dataset_source, backup_host
                    HAVING backup_timestamp = max(backup_timestamp)) AS b
                  ON a.backup_id = b.backup_id
                  GROUP BY a.dataset_source, a.dataset_id) AS d
                GROUP BY d.dataset_source
                ORDER BY d.dataset_source ASC
`,
}

// AddDataset adds metadata about a successful backup of a Dataset,
// along with its parent Backup object.
func (s *Service) AddDataset(ctx context.Context, backup tabacco.Backup, ds tabacco.Dataset) error {
	return retryBusy(ctx, func() error {
		return sqlutil.WithTx(ctx, s.db, func(tx *sql.Tx) error {
			for _, dbAtom := range makeAtoms(backup, ds) {
				stmt := statements["insert_atom"]
				_, err := tx.Exec(
					stmt,
					dbAtom.BackupID,
					dbAtom.BackupTimestamp,
					dbAtom.BackupHost,
					dbAtom.DatasetID,
					dbAtom.DatasetSnapshotID,
					dbAtom.DatasetSource,
					dbAtom.DatasetTotalFiles,
					dbAtom.DatasetTotalBytes,
					dbAtom.DatasetBytesAdded,
					dbAtom.DatasetDuration,
					dbAtom.AtomName,
					dbAtom.AtomPath,
					dbAtom.AtomFullPath,
				)
				if err != nil {
					return err
				}
			}
			return nil
		})
	})
}

// FindAtoms searches for atoms meeting a particular criteria and
// returns them grouped by backup and dataset (the atoms will be
// contained within the dataset).
func (s *Service) FindAtoms(ctx context.Context, req *tabacco.FindRequest) ([]*tabacco.Backup, error) {
	// Build the SQL query. Assemble the WHERE clauses and their
	// respective arguments first.
	var where []string
	var args []interface{}
	if req.Host != "" {
		where = append(where, "backup_host = ?")
		args = append(args, req.Host)
	}
	if req.Pattern != "" {
		where = append(where, "atom_full_path LIKE ?")
		args = append(args, strings.Replace(req.Pattern, "*", "%", -1))
	}
	if !req.OlderThan.IsZero() {
		where = append(where, "backup_timestamp < ?")
		args = append(args, req.OlderThan)
	}

	// Build the final query and execute it.
	q := fmt.Sprintf(
		`SELECT
		   backup_id, backup_timestamp, backup_host,
		   dataset_id, dataset_snapshot_id, dataset_source,
		   dataset_total_files, dataset_total_bytes, dataset_bytes_added,
		   dataset_duration,
		   atom_name, atom_path, atom_full_path
		 FROM log WHERE %s
                 ORDER BY backup_timestamp DESC`,
		strings.Join(where, " AND "),
	)

	var atoms []*dbAtom
	err := sqlutil.WithReadonlyTx(ctx, s.db, func(tx *sql.Tx) error {
		rows, err := tx.Query(q, args...)
		if err != nil {
			return err
		}
		defer rows.Close() // nolint

		for rows.Next() {
			var a dbAtom
			if err := rows.Scan(
				&a.BackupID, &a.BackupTimestamp, &a.BackupHost,
				&a.DatasetID, &a.DatasetSnapshotID, &a.DatasetSource,
				&a.DatasetTotalFiles, &a.DatasetTotalBytes,
				&a.DatasetBytesAdded, &a.DatasetDuration,
				&a.AtomName, &a.AtomPath, &a.AtomFullPath,
			); err != nil {
				log.Printf("bad row: %v", err)
				continue
			}
			atoms = append(atoms, &a)
		}
		return rows.Err()
	})
	if err != nil {
		return nil, err
	}

	return groupByBackup(keepNumVersions(atoms, req.NumVersions)), nil
}

func (s *Service) purge() error {
	cutoff := time.Now().Add(time.Duration(-s.maxAge) * 24 * time.Hour)

	ctx := context.Background()
	return retryBusy(ctx, func() error {
		return sqlutil.WithTx(ctx, s.db, func(tx *sql.Tx) error {
			_, err := tx.Exec(
				`DELETE FROM log WHERE backup_timestamp < ?`,
				cutoff,
			)
			return err
		})
	})
}
