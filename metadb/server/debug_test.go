package server

import (
	"io"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

func TestDebugPages(t *testing.T) {
	defer os.Remove(DBFILE)
	svc, err := New("sqlite3", DBFILE, 0)
	if err != nil {
		t.Fatal(err)
	}
	defer svc.Close()

	addTestEntry(t, svc, "1234", "host1", "file/dataset1")

	httpSrv := newHTTPServer(svc)
	srv := httptest.NewServer(httpSrv.Handler())

	// Main debug page.
	resp, err := http.Get(srv.URL)
	if err != nil {
		t.Fatalf("Get(/): %v", err)
	}
	if resp.StatusCode != 200 {
		t.Errorf("Get(/): %s", resp.Status)
		io.Copy(os.Stderr, resp.Body) // nolint
	}
	resp.Body.Close()

	// Backup debug page.
	resp, err = http.Get(srv.URL + "/backup/by_id?id=1234")
	if err != nil {
		t.Fatalf("Get(/backup/by_id): %v", err)
	}
	if resp.StatusCode != 200 {
		t.Errorf("Get(/backup/by_id): %s", resp.Status)
		io.Copy(os.Stderr, resp.Body) // nolint
	}
	resp.Body.Close()

	// Source debug page.
	resp, err = http.Get(srv.URL + "/dataset/by_source?source=file")
	if err != nil {
		t.Fatalf("Get(/dataset/by_source): %v", err)
	}
	if resp.StatusCode != 200 {
		t.Errorf("Get(/dataset/by_source): %s", resp.Status)
		io.Copy(os.Stderr, resp.Body) // nolint
	}
	resp.Body.Close()

	// Source list debug page.
	resp, err = http.Get(srv.URL + "/sources")
	if err != nil {
		t.Fatalf("Get(/sources): %v", err)
	}
	if resp.StatusCode != 200 {
		t.Errorf("Get(/sources): %s", resp.Status)
		io.Copy(os.Stderr, resp.Body) // nolint
	}
	resp.Body.Close()
}
