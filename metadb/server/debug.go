package server

import (
	"bytes"
	"context"
	"database/sql"
	"fmt"
	"html/template"
	"io"
	"log"
	"net/http"
	"strconv"
	"time"

	"git.autistici.org/ai3/go-common/sqlutil"
	humanize "github.com/dustin/go-humanize"
)

var (
	headerHTML = `<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Tabacco - Debug</title>
    <style type="text/css">
body { background: white; font-family: "Helvetica", sans-serif; }
table th { text-align: left; font-weight: bold; }
table td { text-align: left; padding-right: 10px; }
table thead tr { border-bottom: 2px solid #333; }
.navlink { padding-right: 1em; }
.list-size-box { font-size: 80%; }
.float-right { float: right; padding: 0.3em; }
    </style>
  </head>
  <body>

  <div class="float-right list-size-box">
    <form action="" method="get">
      {{range $k, $v := .RequestArgs}}
      <input type="hidden" name="{{$k}}" value="{{$v}}">
      {{end}}
      <select name="sz">
        {{$list_size := .ListSize}}
        {{range $idx, $value := .ListSizeOptions}}
        <option value="{{$value}}"{{if eq $value $list_size}} selected{{end}}>{{$value}}</option>
        {{end}}
      </select>
      <button type="submit">Update</button>
    </form>
  </div>

  <h1>Tabacco</h1>

  <p>Started at {{fmtDate .StartTime}}.</p>
`

	footerHTML = `
 <p></p>
 <p>
 {{if ne . "home"}}
   <a class="navlink" href="/">Latest backups</a>
 {{end}}
 {{if ne . "sources"}}
   <a class="navlink" href="/sources">Sources</a>
 {{end}}
 </p>
</body>
</html>`

	datasetsHTML = `{{template "header" .}}

{{if .ShowActiveJobs}}
{{template "active_jobs" .}}
{{end}}

  <h3>{{.Title}}</h3>

  <table>
   <thead>
    <tr>
     <th>ID</th>
     <th>Time</th>
     <th>Host</th>
     <th>Dataset</th>
     <th>Source</th>
     <th>Atoms</th>
     <th>Files</th>
     <th>Size</th>
     <th>Time</th>
    </tr>
   </thead>
   <tbody>
    {{range $outidx, $b := .Backups}}
    {{range $idx, $d := $b.Datasets}}
    <tr>
     {{if eq $idx 0}}
     <td><b><a href="/backup/by_id?id={{$b.BackupID}}">
              {{$b.BackupID}}</a></b></td>
     <td>{{fmtDate $b.BackupTimestamp}}</td>
     <td><a href="/dataset/by_host?host={{$b.BackupHost}}">
           {{$b.BackupHost}}</a></td>
     {{else}}
     <td colspan="3"></td>
     {{end}}
     <td><a href="/dataset/by_id?id={{$d.DatasetID}}">
           {{$d.DatasetID}}</a></td>
     <td><a href="/dataset/by_source?source={{$d.DatasetSource}}">
           {{$d.DatasetSource}}</a></td>
     <td>{{$d.NumAtoms}}</td>
     <td>{{$d.DatasetTotalFiles}}</td>
     <td>{{humanBytes $d.DatasetTotalBytes}}
         ({{humanBytes $d.DatasetBytesAdded}} new)</td>
     <td>{{$d.DatasetDuration}}s</td>
    </tr>
    {{end}}
    {{end}}
   </tbody>
  </table>

{{if .ShowActiveJobs}}
{{template "footer" "home"}}
{{else}}
{{template "footer" "dataset"}}
{{end}}`

	atomsHTML = `{{template "header" .}}

  <h3>Source: {{.Dataset.DatasetSource}}</h3>

  <table>
   <tbody>
    <tr>
     <td>Backup ID</td>
     <td><b><a href="/backup/by_id?id={{.Backup.BackupID}}">
             {{.Backup.BackupID}}</a></b></td>
    </tr>
    <tr>
     <td>Timestamp</td>
     <td>{{fmtDate .Backup.BackupTimestamp}}</td>
    </tr>
    <tr>
     <td>Host</td>
     <td>{{.Backup.BackupHost}}</td>
    </tr>
    <tr>
     <td>Files in dataset</td>
     <td>{{.Dataset.DatasetTotalFiles}}</td>
    </tr>
    <tr>
     <td>Bytes in dataset</td>
     <td>{{humanBytes .Dataset.DatasetTotalBytes}}
         ({{humanBytes .Dataset.DatasetBytesAdded}} new)</td>
    </tr>
    <tr>
     <td>Time elapsed</td>
     <td>{{.Dataset.DatasetDuration}} seconds</td>
    </tr>
   </tbody>
  </table>

  <h3>Atoms</h3>

  <table>
   <thead>
    <tr>
     <th>Name</th>
     <th>Path</th>
    </tr>
   </thead>
   <tbody>
    {{range .Atoms}}
    <tr>
     <td>{{.AtomName}}</td>
     <td>{{if .AtomPath}}{{.AtomPath}}{{else}}<i>stdin</i>{{end}}</td>
    </tr>
    {{end}}
   </tbody>
  </table>

{{template "footer" "source"}}`

	allSourcesHTML = `{{template "header" .}}

  <h3>Latest backups by source</h3>

  <table>
   <thead>
    <tr>
     <th>ID</th>
     <th>Time</th>
     <th>Host</th>
     <th>Source</th>
    </tr>
   </thead>
   <tbody>
    {{range $idx, $b := .Backups}}
    <tr>
     <td><b><a href="/backup/by_id?id={{$b.BackupID}}">
              {{$b.BackupID}}</a></b></td>
     <td>{{fmtDate $b.BackupTimestamp}}</td>
     <td><a href="/dataset/by_host?host={{$b.BackupHost}}">
           {{$b.BackupHost}}</a></td>
     <td><a href="/dataset/by_source?source={{$b.DatasetSource}}">
           {{$b.DatasetSource}}</a></td>
    </tr>
    {{end}}
   </tbody>
  </table>

  <h3>Backup size by source</h3>

  <table>
   <thead>
    <tr>
     <th>Source</th>
     <th>Size</th>
    </tr>
   </thead>
   <tbody>
    {{range $idx, $b := .SourcesBySize}}
    <tr>
     <td><a href="/dataset/by_source?source={{$b.DatasetSource}}">
           {{$b.DatasetSource}}</a></td>
     <td><b>{{humanBytes $b.Size}}</td>
    </tr>
    {{end}}
   </tbody>
  </table>

{{template "footer" "sources"}}`

	activeJobsHTML = `

  <h3>Actively running jobs</h3>

{{if .ActiveJobs}}
  <table>
   <thead>
    <tr>
     <th>Host</th>
     <th>Backup ID</th>
     <th>Source</th>
     <th>Details</th>
    </tr>
   </thead>
   <tbody>
    {{range $id, $a := .ActiveJobs}}
    <tr>
     <td>{{$a.Host}}</td>
     <td>{{$a.BackupID}}</td>
     <td>{{$a.DatasetSource}}</td>
     {{if $a.Status}}
     <td>
       <small>
         {{fmtPercent $a.Status.PercentDone}}% done, {{$a.Status.SecondsElapsed}}s elapsed<br>
         {{if $a.Status.FilesDone}}
           {{$a.Status.FilesDone}}/{{$a.Status.TotalFiles}} files<br>
         {{end}}
       </small>
     </td>
     {{if $a.Status.CurrentFiles}}
    </tr>
    <tr>
     <td colspan="5">
       {{range $a.Status.CurrentFiles}}
         {{.}}<br>
       {{end}}
     </td>
     {{end}}
    </tr>
    {{end}}
    {{end}}
   </tbody>
  </table>
{{else}}
  <p>No backup jobs currently running.</p>
{{end}}
`

	debugTemplate *template.Template

	startTime time.Time
)

func init() {
	funcs := map[string]interface{}{
		"fmtDate":    fmtDate,
		"fmtPercent": fmtPercent,
		"humanBytes": func(i int64) string {
			return humanize.Bytes(uint64(i))
		},
	}
	debugTemplate = template.New("").Funcs(funcs)
	template.Must(debugTemplate.New("header").Parse(headerHTML))
	template.Must(debugTemplate.New("footer").Parse(footerHTML))
	template.Must(debugTemplate.New("active_jobs").Parse(activeJobsHTML))
	template.Must(debugTemplate.New("datasets").Parse(datasetsHTML))
	template.Must(debugTemplate.New("source").Parse(atomsHTML))
	template.Must(debugTemplate.New("all_sources").Parse(allSourcesHTML))
	startTime = time.Now()
}

const (
	listSizeCookieName = "list_size"
	defaultListSize    = 30
)

type listSizeKeyType int

var (
	listSizeKey     listSizeKeyType
	listSizeOptions = []int{30, 50, 100, 300}
)

func requestArgs(r *http.Request) map[string]string {
	out := make(map[string]string)
	// Assume ParseForm() has been called already.
	for key, value := range r.Form {
		if key != "sz" && len(value) > 0 {
			out[key] = value[0]
		}
	}
	return out
}

func withListSize(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		sz := defaultListSize
		if cookie, err := r.Cookie(listSizeCookieName); err == nil {
			if i, err := strconv.Atoi(cookie.Value); err == nil {
				sz = i
			}
		}
		if s := r.FormValue("sz"); s != "" {
			if i, err := strconv.Atoi(s); err == nil {
				sz = i
				http.SetCookie(w, &http.Cookie{
					Name:  listSizeCookieName,
					Value: strconv.Itoa(sz),
					Path:  "/",
				})
			}
		}

		r = r.WithContext(context.WithValue(r.Context(), listSizeKey, sz))
		h.ServeHTTP(w, r)
	})
}

func getListSize(r *http.Request) int {
	sz := defaultListSize
	if i, ok := r.Context().Value(listSizeKey).(int); ok {
		sz = i
	}
	return sz
}

func executeDebugTemplate(w http.ResponseWriter, r *http.Request, name string, data map[string]interface{}) {
	data["StartTime"] = startTime
	data["ListSize"] = getListSize(r)
	data["ListSizeOptions"] = listSizeOptions
	data["RequestArgs"] = requestArgs(r)

	var buf bytes.Buffer
	if err := debugTemplate.Lookup(name).Execute(&buf, data); err != nil {
		log.Printf("debug template error: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Length", strconv.Itoa(buf.Len()))
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	io.Copy(w, &buf) // nolint: errcheck
}

type datasetDebug struct {
	DatasetID         string
	DatasetSnapshotID string
	DatasetSource     string
	DatasetTotalFiles int64
	DatasetTotalBytes int64
	DatasetBytesAdded int64
	DatasetDuration   int

	NumAtoms int
}

type backupDebug struct {
	BackupID        string
	BackupTimestamp time.Time
	BackupHost      string

	Datasets []*datasetDebug
}

func readDatasets(rows *sql.Rows) ([]*backupDebug, error) {
	tmp := make(map[string]*backupDebug)
	var backups []*backupDebug

	for rows.Next() {
		var bd backupDebug
		var dd datasetDebug
		if err := rows.Scan(
			&bd.BackupID, &bd.BackupTimestamp, &bd.BackupHost,
			&dd.DatasetID, &dd.DatasetSnapshotID, &dd.DatasetSource,
			&dd.DatasetTotalFiles, &dd.DatasetTotalBytes,
			&dd.DatasetBytesAdded, &dd.DatasetDuration, &dd.NumAtoms,
		); err != nil {
			return nil, err
		}

		b, ok := tmp[bd.BackupID]
		if !ok {
			b = &bd
			tmp[bd.BackupID] = b
			backups = append(backups, b)
		}
		b.Datasets = append(b.Datasets, &dd)
	}

	return backups, rows.Err()
}

func (s *httpServer) queryDatasets(ctx context.Context, queryName string, queryArgs ...interface{}) (map[string]interface{}, error) {
	var backups []*backupDebug

	if err := retryBusy(ctx, func() error {
		return sqlutil.WithReadonlyTx(ctx, s.db, func(tx *sql.Tx) error {
			stmt := statements[queryName]
			rows, err := tx.Query(stmt, queryArgs...)
			if err != nil {
				return err
			}
			defer rows.Close()
			backups, err = readDatasets(rows)
			return err
		})
	}); err != nil {
		return nil, err
	}

	return map[string]interface{}{
		"Backups": backups,
	}, nil
}

func (s *httpServer) showDatasets(w http.ResponseWriter, r *http.Request, title string, data map[string]interface{}, err error) {
	if err != nil {
		log.Printf("debug error: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	data["Title"] = title
	executeDebugTemplate(w, r, "datasets", data)
}

func (s *httpServer) withActiveJobs(data map[string]interface{}) map[string]interface{} {
	if data == nil {
		return nil
	}
	data["ShowActiveJobs"] = true
	data["ActiveJobs"] = s.agents.getActiveJobs()
	return data
}

func (s *httpServer) handleDebug(w http.ResponseWriter, r *http.Request) {
	data, err := s.queryDatasets(r.Context(), "get_latest_datasets", getListSize(r))
	s.showDatasets(w, r, "Latest backups", s.withActiveJobs(data), err)
}

func (s *httpServer) handleDebugDatasetsBySource(w http.ResponseWriter, r *http.Request) {
	source := r.FormValue("source")
	if source == "" {
		http.Error(w, "No source specified", http.StatusBadRequest)
		return
	}

	data, err := s.queryDatasets(r.Context(), "get_latest_datasets_by_source", source, getListSize(r))
	s.showDatasets(w, r, "Latest backups for "+source, data, err)
}

func (s *httpServer) handleDebugDatasetsByHost(w http.ResponseWriter, r *http.Request) {
	host := r.FormValue("host")
	if host == "" {
		http.Error(w, "No host specified", http.StatusBadRequest)
		return
	}

	data, err := s.queryDatasets(r.Context(), "get_latest_datasets_by_host", host, getListSize(r))
	s.showDatasets(w, r, "Latest backups for host "+host, data, err)
}

func (s *httpServer) handleDebugBackupByID(w http.ResponseWriter, r *http.Request) {
	id := r.FormValue("id")
	if id == "" {
		http.Error(w, "No id specified", http.StatusBadRequest)
		return
	}

	data, err := s.queryDatasets(r.Context(), "get_backup_by_id", id)
	s.showDatasets(w, r, "Backup "+id, data, err)
}

func (s *httpServer) handleDebugDatasetByID(w http.ResponseWriter, r *http.Request) {
	id := r.FormValue("id")
	if id == "" {
		http.Error(w, "No id specified", http.StatusBadRequest)
		return
	}

	var atoms []*dbAtom
	err := retryBusy(r.Context(), func() error {
		return sqlutil.WithReadonlyTx(r.Context(), s.db, func(tx *sql.Tx) error {
			stmt := statements["get_dataset_by_id"]
			rows, err := tx.Query(stmt, id)
			if err != nil {
				return err
			}
			defer rows.Close()

			for rows.Next() {
				var atom dbAtom
				if err := rows.Scan(
					&atom.BackupID, &atom.BackupTimestamp, &atom.BackupHost,
					&atom.DatasetID, &atom.DatasetSnapshotID, &atom.DatasetSource,
					&atom.DatasetTotalFiles, &atom.DatasetTotalBytes,
					&atom.DatasetBytesAdded, &atom.DatasetDuration,
					&atom.AtomName, &atom.AtomPath, &atom.AtomFullPath,
				); err != nil {
					return err
				}
				atoms = append(atoms, &atom)
			}

			return rows.Err()
		})
	})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if len(atoms) == 0 {
		http.NotFound(w, r)
		return
	}

	executeDebugTemplate(w, r, "source", map[string]interface{}{
		"Atoms":   atoms,
		"Backup":  atoms[0],
		"Dataset": atoms[0],
	})
}

type latestBackup struct {
	BackupID        string
	BackupTimestamp time.Time
	BackupHost      string
	DatasetSource   string
}

type sourceSize struct {
	DatasetSource string
	Size          int64
}

func (s *httpServer) fetchLatestBackupsBySource(ctx context.Context) ([]*latestBackup, error) {
	var out []*latestBackup
	err := retryBusy(ctx, func() error {
		return sqlutil.WithReadonlyTx(ctx, s.db, func(tx *sql.Tx) error {
			stmt := statements["get_latest_backups_by_source"]
			rows, err := tx.Query(stmt)
			if err != nil {
				return err
			}
			defer rows.Close()
			for rows.Next() {
				var lb latestBackup
				if err := rows.Scan(
					&lb.BackupID, &lb.BackupTimestamp, &lb.BackupHost, &lb.DatasetSource,
				); err != nil {
					return err
				}
				out = append(out, &lb)
			}
			return rows.Err()
		})
	})
	return out, err
}

func (s *httpServer) fetchSourcesSize(ctx context.Context) ([]*sourceSize, error) {
	var out []*sourceSize
	err := retryBusy(ctx, func() error {
		return sqlutil.WithReadonlyTx(ctx, s.db, func(tx *sql.Tx) error {
			stmt := statements["get_sources_size"]
			rows, err := tx.Query(stmt)
			if err != nil {
				return err
			}
			defer rows.Close()
			for rows.Next() {
				var s sourceSize
				if err := rows.Scan(
					&s.DatasetSource, &s.Size,
				); err != nil {
					return err
				}
				out = append(out, &s)
			}
			return rows.Err()
		})
	})
	return out, err
}

func (s *httpServer) handleDebugAllSources(w http.ResponseWriter, r *http.Request) {
	latest, err := s.fetchLatestBackupsBySource(r.Context())
	if err != nil {
		log.Printf("debug(/sources): error: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	sizes, err := s.fetchSourcesSize(r.Context())
	if err != nil {
		log.Printf("debug(/sources): error: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	executeDebugTemplate(w, r, "all_sources", map[string]interface{}{
		"Backups":       latest,
		"SourcesBySize": sizes,
	})
}

func fmtDate(t time.Time) string {
	return t.Format(time.RFC3339)
}

func fmtPercent(ratio float64) string {
	return fmt.Sprintf("%.3g", 100.0*ratio)
}
