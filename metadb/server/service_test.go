package server

import (
	"context"
	"fmt"
	"os"
	"testing"
	"time"

	"git.autistici.org/ai3/tools/tabacco"
)

const DBFILE = ".service_test.db"

func addTestEntry(t *testing.T, svc *Service, backupID, host, dsName string) {
	err := svc.AddDataset(
		context.Background(),
		tabacco.Backup{
			ID:        backupID,
			Host:      host,
			Timestamp: time.Now(),
		},
		tabacco.Dataset{
			ID:     "ds" + backupID,
			Source: "file",
			Atoms: []tabacco.Atom{
				{
					Name: dsName + "/sub1",
					Path: "/path/dataset1/sub1",
				},
				{
					Name: dsName + "/sub2",
					Path: "/path/dataset1/sub2",
				},
			},
			TotalBytes: 42,
			TotalFiles: 2,
			BytesAdded: 42,
			Duration:   1,
		},
	)
	if err != nil {
		t.Fatal("AddDataset", err)
	}
}

func TestService_AddDataset(t *testing.T) {
	defer os.Remove(DBFILE)
	svc, err := New("sqlite3", DBFILE, 0)
	if err != nil {
		t.Fatal(err)
	}
	defer svc.Close()

	addTestEntry(t, svc, "1234", "host1", "file/dataset1")
}

func TestService_FindAtoms(t *testing.T) {
	defer os.Remove(DBFILE)
	svc, err := New("sqlite3", DBFILE, 0)
	if err != nil {
		t.Fatal(err)
	}
	defer svc.Close()

	// Create 10 fake backups, which differ only in host.
	for i := 0; i < 10; i++ {
		addTestEntry(t, svc, fmt.Sprintf("backup%06d", i), fmt.Sprintf("host%d", i), "file/dataset1")
	}

	// Searching for a specific atom (common to all backups)
	// should return exactly 10 results.
	vv, err := svc.FindAtoms(
		context.Background(),
		&tabacco.FindRequest{
			Pattern:     "*/sub1",
			NumVersions: 10,
		},
	)
	if err != nil {
		t.Fatal("FindAtoms(1)", err)
	}
	if len(vv) != 10 {
		t.Fatalf("bad result(1): %+v", vv)
	}

	// Augmenting the previous search with a host filter should
	// return a single result.
	vv, err = svc.FindAtoms(
		context.Background(),
		&tabacco.FindRequest{
			Pattern: "*/sub1",
			Host:    "host7",
		},
	)
	if err != nil {
		t.Fatal("FindAtoms(2)", err)
	}
	if len(vv) != 1 {
		t.Fatalf("bad result(2): %+v", vv)
	}
}
