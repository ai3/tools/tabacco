package server

import (
	"sort"
	"sync"
	"time"

	"git.autistici.org/ai3/tools/tabacco"
)

var (
	cleanupTime     = 20 * time.Minute
	cleanupInterval = 5 * time.Minute
)

type statusEntry struct {
	*tabacco.JobStatus
	timestamp time.Time
}

// Map that holds the agents' status, on each host. Hosts that stop
// reporting their status disappear from this map after a few minutes.
type statusMap struct {
	mx     sync.Mutex
	agents map[string]*statusEntry
}

func newStatusMap() *statusMap {
	m := &statusMap{
		agents: make(map[string]*statusEntry),
	}
	go m.cleanupLoop()
	return m
}

func (m *statusMap) cleanupLoop() {
	ticker := time.NewTicker(cleanupInterval)
	for range ticker.C {
		m.cleanup()
	}
}

func (m *statusMap) cleanup() {
	deadline := time.Now().Add(-cleanupTime)
	m.mx.Lock()
	var toDelete []string
	for id, entry := range m.agents {
		if entry.timestamp.Before(deadline) {
			toDelete = append(toDelete, id)
		}
	}
	for _, id := range toDelete {
		delete(m.agents, id)
	}
	m.mx.Unlock()
}

func (m *statusMap) update(up *tabacco.UpdateActiveJobStatusRequest) {
	m.mx.Lock()
	for _, status := range up.ActiveJobs {
		m.agents[status.Host] = &statusEntry{
			JobStatus: status,
			timestamp: time.Now(),
		}
	}
	m.mx.Unlock()
}

func (m *statusMap) getActiveJobs() []*tabacco.JobStatus {
	m.mx.Lock()
	out := make([]*tabacco.JobStatus, 0, len(m.agents))
	for _, a := range m.agents {
		out = append(out, a.JobStatus)
	}
	m.mx.Unlock()

	sort.Sort(jobStatusList(out))

	return out
}

type jobStatusList []*tabacco.JobStatus

func (l jobStatusList) Len() int      { return len(l) }
func (l jobStatusList) Swap(i, j int) { l[i], l[j] = l[j], l[i] }
func (l jobStatusList) Less(i, j int) bool {
	if l[i].Host == l[j].Host {
		return l[i].BackupID < l[j].BackupID
	}
	return l[i].Host < l[j].Host
}
