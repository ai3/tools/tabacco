package tabacco

import (
	"context"
	"fmt"
	"io/ioutil"
	"path/filepath"

	"git.autistici.org/ai3/tools/tabacco/jobs"
)

type fileHandler struct {
	path    string
	exclude []string
}

func newFileHandler(name string, params Params) (Handler, error) {
	return &fileHandler{
		path:    params.Get("path"),
		exclude: params.GetList("exclude"),
	}, nil
}

// Convert the atom to a path.
func atomPath(a Atom, root string) string {
	// If the atom has a path, use that.
	if a.Path != "" {
		// If it's an absolute path, just use it.
		if a.Path[0] == '/' {
			return a.Path
		}
		// Otherwise join it with the root path.
		return filepath.Join(root, a.Path)
	}
	// Join the name with the root path by default.
	return filepath.Join(root, a.Name)
}

func (h *fileHandler) saveAtomsToFile(ctx context.Context, atoms []Atom) (string, error) {
	tmpf, err := ioutil.TempFile(getWorkDir(ctx), "file-list-")
	if err != nil {
		return "", err
	}
	for _, a := range atoms {
		path := atomPath(a, h.path)
		if _, err := fmt.Fprintf(tmpf, "%s\n", path); err != nil {
			return "", err
		}
	}
	return tmpf.Name(), tmpf.Close()
}

func (h *fileHandler) BackupJob(rctx RuntimeContext, backup *Backup, ds *Dataset) jobs.Job {
	return jobs.JobFunc(func(ctx context.Context) error {
		// Build the list of filesystem paths to pass to the
		// Repository.Backup method. Use a temporary file, in case
		// the path list is too long for the command line.
		tmpf, err := h.saveAtomsToFile(ctx, ds.Atoms)
		if err != nil {
			return err
		}

		// Invoke the backup command (path-based).
		return rctx.Repo().RunBackup(ctx, rctx.Shell(), backup, ds, tmpf, h.exclude)
	})
}

func (h *fileHandler) RestoreJob(rctx RuntimeContext, backup *Backup, ds *Dataset, target string) jobs.Job {
	// Build the list of filesystem paths to pass to the
	// Repository.Backup method.
	var paths []string
	for _, a := range ds.Atoms {
		paths = append(paths, atomPath(a, h.path))
	}

	// Call the repo Restore method.
	return jobs.JobFunc(func(ctx context.Context) error {
		return rctx.Repo().RunRestore(ctx, rctx.Shell(), backup, ds, paths, target)
	})
}
