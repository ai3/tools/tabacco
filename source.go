package tabacco

import (
	"context"
	"errors"
	"fmt"
	"os"
	"os/exec"
	"time"

	"git.autistici.org/ai3/tools/tabacco/util"
	"gopkg.in/yaml.v3"
)

// DatasetSpec describes a dataset in the configuration.
type DatasetSpec struct {
	//Name string `yaml:"name"`

	Atoms        []Atom `yaml:"atoms"`
	AtomsCommand string `yaml:"atoms_command"`
}

// Parse a DatasetSpec and return a Dataset.
func (spec *DatasetSpec) Parse(ctx context.Context, src *SourceSpec) (*Dataset, error) {
	// Build the atoms list, invoking the atoms_command if necessary.
	var atoms []Atom
	atoms = append(atoms, spec.Atoms...)
	if spec.AtomsCommand != "" {
		var cmdAtoms []Atom
		if err := runYAMLCommand(ctx, spec.AtomsCommand, &cmdAtoms); err != nil {
			return nil, fmt.Errorf("source %s: error in atoms command: %v", src.Name, err)
		}
		atoms = append(atoms, cmdAtoms...)
	}

	return &Dataset{
		ID:     util.RandomID(),
		Source: src.Name,
		Atoms:  atoms,
	}, nil
}

// Check syntactical validity of the DatasetSpec.
func (spec *DatasetSpec) Check() error {
	if len(spec.Atoms) > 0 && spec.AtomsCommand != "" {
		return errors.New("can't specify both 'atoms' and 'atoms_command'")
	}
	if len(spec.Atoms) == 0 && spec.AtomsCommand == "" {
		return errors.New("must specify one of 'atoms' or 'atoms_command'")
	}
	return nil
}

// SourceSpec defines the configuration for a data source. Data
// sources can dynamically or statically generate one or more
// Datasets, each containing one or more Atoms.
//
// Handlers are launched once per Dataset, and they know how to deal
// with backing up / restoring individual Atoms.
type SourceSpec struct {
	Name    string `yaml:"name"`
	Handler string `yaml:"handler"`

	// Schedule to run the backup on.
	Schedule string `yaml:"schedule"`

	// Define Datasets statically, or use a script to generate them
	// dynamically on every new backup.
	Datasets        []*DatasetSpec `yaml:"datasets"`
	DatasetsCommand string         `yaml:"datasets_command"`

	// Commands to run before and after operations on the source.
	PreBackupCommand   string `yaml:"pre_backup_command"`
	PostBackupCommand  string `yaml:"post_backup_command"`
	PreRestoreCommand  string `yaml:"pre_restore_command"`
	PostRestoreCommand string `yaml:"post_restore_command"`

	Params Params `yaml:"params"`

	// Timeout for execution of the entire backup operation.
	Timeout time.Duration `yaml:"timeout"`
}

// Parse a SourceSpec and return one or more Datasets.
func (spec *SourceSpec) Parse(ctx context.Context) ([]*Dataset, error) {
	// Build the atoms list, invoking the atoms_command if
	// necessary, and creating actual atoms with absolute names.
	dspecs := append([]*DatasetSpec{}, spec.Datasets...)
	if spec.DatasetsCommand != "" {
		var cmdSpecs []*DatasetSpec
		if err := runYAMLCommand(ctx, spec.DatasetsCommand, &cmdSpecs); err != nil {
			return nil, fmt.Errorf("error in datasets command: %v", err)
		}
		dspecs = append(dspecs, cmdSpecs...)
	}

	// Call Parse on all datasets.
	datasets := make([]*Dataset, 0, len(dspecs))
	for _, dspec := range dspecs {
		ds, err := dspec.Parse(ctx, spec)
		if err != nil {
			return nil, fmt.Errorf("error parsing dataset: %v", err)
		}
		datasets = append(datasets, ds)
	}
	return datasets, nil
}

// Check syntactical validity of the SourceSpec. Not an alternative to
// validation at usage time, but it provides an early warning to the
// user. Checks the handler name against a string set of handler
// names.
func (spec *SourceSpec) Check(handlers map[string]*HandlerSpec) error {
	if spec.Timeout == 0 {
		spec.Timeout = 24 * time.Hour
	}

	if spec.Name == "" {
		return errors.New("source name is not set")
	}
	if spec.Schedule == "" {
		return errors.New("schedule is not set")
	}
	if spec.Handler == "" {
		return errors.New("handler is not set")
	}
	if _, ok := handlers[spec.Handler]; !ok {
		return fmt.Errorf("unknown handler '%s'", spec.Handler)
	}
	if len(spec.Datasets) > 0 && spec.DatasetsCommand != "" {
		return errors.New("can't specify both 'datasets' and 'datasets_command'")
	}
	if len(spec.Datasets) == 0 && spec.DatasetsCommand == "" {
		return errors.New("must specify one of 'datasets' or 'datasets_command'")
	}

	// Check the datasets, at least those that are provided
	// statically.
	merr := new(util.MultiError)
	for _, ds := range spec.Datasets {
		if err := ds.Check(); err != nil {
			merr.Add(err)
		}
	}
	return merr.OrNil()
}

func runYAMLCommand(ctx context.Context, cmd string, obj interface{}) error {
	c := exec.Command("/bin/sh", "-c", cmd) // #nosec
	c.Stderr = os.Stderr
	output, err := c.Output()
	if err != nil {
		return err
	}

	return yaml.Unmarshal(output, obj)
}
