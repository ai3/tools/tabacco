package tabacco

import (
	"context"
	"errors"
	"fmt"
	"log"
	"os"
	"testing"
	"time"
)

func TestReadConfig(t *testing.T) {
	conf, err := ReadConfig("testdata/agent.yml")
	if err != nil {
		t.Fatal("ReadConfig()", err)
	}
	if l := len(conf.SourceSpecs); l != 1 {
		t.Fatalf("couldn't read all SourceSpecs: got %d, expected %d", l, 1)
	}
	if l := len(conf.HandlerSpecs); l != 3 {
		t.Fatalf("couldn't read all HandlerSpecs: got %d, expected %d", l, 3)
	}
}

func TestConfigManager(t *testing.T) {
	conf, err := ReadConfig("testdata/agent.yml")
	if err != nil {
		t.Fatal("ReadConfig()", err)
	}
	log.Printf("loaded %d sources", len(conf.SourceSpecs))
	mgr, err := NewConfigManager(conf)
	if err != nil {
		t.Fatal("NewConfigManager()", err)
	}
	defer mgr.Close()

	// Test one of the accessor methods.
	if s := mgr.current().SourceSpecs(); len(s) != 1 {
		t.Fatalf("current().SourceSpecs() bad result: %+v", s)
	}

	// Test the Notify() mechanism by checking that it triggers
	// right away when setting up a new listener.
	tmr := time.NewTimer(1 * time.Second)
	select {
	case <-mgr.Notify():
	case <-tmr.C:
		t.Fatal("Notify() channel did not trigger")
	}
}

func TestRandomSeed(t *testing.T) {
	seedFile := ".test_seed"
	defer os.Remove(seedFile) // nolint

	seed := mustGetSeed(seedFile)
	if seed == 0 {
		t.Fatal("seed is zero")
	}
	seed2 := mustGetSeed(seedFile)
	if seed2 != seed {
		t.Fatal("failed to persist random seed")
	}
}

func TestConfig_Parse(t *testing.T) {
	type testdata struct {
		config     *Config
		expectedOK bool
		checkFn    func([]*Dataset) error
	}
	tdd := []testdata{
		// The following tests cover a few ways to generate
		// the same two "user account" atoms as outlined in
		// the README.
		{
			&Config{
				SourceSpecs: []*SourceSpec{
					&SourceSpec{
						Name:     "users",
						Handler:  "file",
						Schedule: "@random_every 24h",
						Datasets: []*DatasetSpec{
							&DatasetSpec{
								Atoms: []Atom{
									{Name: "account1"},
								},
							},
							{
								Atoms: []Atom{
									{Name: "account2"},
								},
							},
						},
					},
				},
				HandlerSpecs: []*HandlerSpec{
					&HandlerSpec{
						Name:   "file",
						Type:   "file",
						Params: map[string]interface{}{"path": "/"},
					},
				},
			},
			true,
			checkTwoUserAccountsAtoms,
		},
		{
			&Config{
				SourceSpecs: []*SourceSpec{
					&SourceSpec{
						Name:     "users",
						Handler:  "file",
						Schedule: "@random_every 24h",
						Datasets: []*DatasetSpec{
							&DatasetSpec{
								Atoms: []Atom{
									{Name: "account1"},
									{Name: "account2"},
								},
							},
						},
					},
				},
				HandlerSpecs: []*HandlerSpec{
					&HandlerSpec{
						Name:   "file",
						Type:   "file",
						Params: map[string]interface{}{"path": "/data"},
					},
				},
			},
			true,
			checkTwoUserAccountsAtoms,
		},
		{
			&Config{
				SourceSpecs: []*SourceSpec{
					&SourceSpec{
						Name:            "users",
						Handler:         "file",
						Schedule:        "@random_every 24h",
						DatasetsCommand: "echo '[{atoms: [{name: account1}, {name: account2}]}]'",
					},
				},
				HandlerSpecs: []*HandlerSpec{
					&HandlerSpec{
						Name:   "file",
						Type:   "file",
						Params: map[string]interface{}{"path": "/data"},
					},
				},
			},
			true,
			checkTwoUserAccountsAtoms,
		},
	}

	for _, td := range tdd {
		// Set a default repository config.
		td.config.Repository.Name = "default"
		td.config.Repository.Type = "restic"
		td.config.Repository.Params = map[string]interface{}{
			"uri":      "/tmp",
			"password": "hello",
		}

		parsed, err := td.config.parse()
		if err != nil && td.expectedOK {
			t.Errorf("unexpected error for config %+v: %v", td.config, err)
		} else if err == nil && !td.expectedOK {
			t.Errorf("missing error for config %+v", td.config)
		} else {
			datasets, err := parseAllSources(parsed.SourceSpecs())
			if err != nil {
				t.Errorf("failed to parse sources %+v: %v", td.config.SourceSpecs, err)
			}
			if td.checkFn != nil {
				if err := td.checkFn(datasets); err != nil {
					t.Errorf("check failed for config %+v: %v", td.config, err)
				}
			}
		}
		if parsed != nil {
			parsed.Close()
		}
	}
}

func parseAllSources(specs []*SourceSpec) ([]*Dataset, error) {
	var out []*Dataset
	for _, spec := range specs {
		ds, err := spec.Parse(context.Background())
		if err != nil {
			return nil, err
		}
		out = append(out, ds...)
	}
	return out, nil
}

func checkTwoUserAccountsAtoms(datasets []*Dataset) error {
	var numAtoms int
	for _, ds := range datasets {
		if ds.ID == "" {
			return errors.New("empty dataset ID")
		}
		for _, atom := range ds.Atoms {
			switch atom.Name {
			case "account1", "account2":
			default:
				return fmt.Errorf("bad atom name: %s", atom.Name)
			}
			numAtoms++
		}
	}
	if numAtoms != 2 {
		return fmt.Errorf("expected 2 atoms across all datasets, got %d atoms across %d datasets", numAtoms, len(datasets))
	}
	return nil
}
