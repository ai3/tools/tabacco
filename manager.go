package tabacco

import (
	"context"
	"errors"
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	"git.autistici.org/ai3/tools/tabacco/jobs"
	"git.autistici.org/ai3/tools/tabacco/util"
)

// Manager for backups and restores.
type tabaccoManager struct {
	*jobs.ExclusiveLockManager
	*jobs.QueueManager
	*jobs.StateManager
	*workdirManager

	configMgr *ConfigManager
	ms        MetadataStore
}

// NewManager creates a new Manager.
func NewManager(ctx context.Context, configMgr *ConfigManager, ms MetadataStore) (Manager, error) {
	// If we can't create a workdirManager, it probably means we
	// don't have permissions to the WorkDir, which is bad.
	wm, err := newWorkdirManager(configMgr.current().WorkDir())
	if err != nil {
		return nil, err
	}

	// Note: the queue configuration won't be reloaded.
	return &tabaccoManager{
		ExclusiveLockManager: jobs.NewExclusiveLockManager(),
		QueueManager:         jobs.NewQueueManager(configMgr.current().QueueSpec()),
		StateManager:         jobs.NewStateManager(),
		workdirManager:       wm,

		configMgr: configMgr,
		ms:        ms,
	}, nil
}

// Close the Manager and free all associated resources (those owned by
// this object).
func (m *tabaccoManager) Close() error {
	m.workdirManager.Close()

	return nil
}

type metadataJob struct {
	jobs.Job
	ms     MetadataStore
	backup *Backup
	ds     *Dataset
}

func (j *metadataJob) RunContext(ctx context.Context) error {
	err := j.Job.RunContext(ctx)
	if err == nil {
		if merr := j.ms.AddDataset(ctx, j.backup, j.ds); merr != nil {
			log.Printf("%s@%s: error saving metadata: %v", j.ds.Source, j.ds.ID, merr)
		}
	}
	return err
}

func (m *tabaccoManager) withMetadata(j jobs.Job, backup *Backup, ds *Dataset) jobs.Job {
	return &metadataJob{
		Job:    j,
		ms:     m.ms,
		backup: backup,
		ds:     ds,
	}
}

// Prepare the repository for a new backup. This is a synchronous
// operation: we need to wait for it to complete to avoid running the
// backup tasks too soon.
func (m *tabaccoManager) prepareBackupJob(rctx RuntimeContext, backup *Backup) jobs.Job {
	// Create a log context
	return jobs.JobFunc(func(ctx context.Context) error {
		return rctx.Repo().Init(ctx, rctx)
		//log.Printf("preparing backup %s", backup.ID)
		//return repo.Prepare(ctx, backup)
	})
}

func (m *tabaccoManager) wrapWithCommands(rctx RuntimeContext, initJob, backupJob jobs.Job, pre, post string) jobs.Job {
	var out []jobs.Job
	if initJob != nil {
		out = append(out, initJob)
	}
	if pre != "" {
		out = append(out, m.commandJob(rctx, pre))
	}
	out = append(out, backupJob)
	if post != "" {
		out = append(out, m.commandJob(rctx, post))
	}

	if len(out) == 1 {
		return out[0]
	}
	return jobs.SyncGroup(out)
}

func (m *tabaccoManager) makeBackupJob(ctx context.Context, rctx RuntimeContext, backup *Backup, src *SourceSpec) (jobs.Job, error) {
	// Compile the source and the associated Handler.
	dsl, err := src.Parse(ctx)
	if err != nil {
		return nil, err
	}
	hspec := rctx.HandlerSpec(src.Handler)
	if hspec == nil {
		return nil, fmt.Errorf("unknown handler '%s'", src.Handler)
	}
	h, err := hspec.Parse(src)
	if err != nil {
		return nil, err
	}

	// The actual backup operation. Assemble all the backup jobs
	// for the datasets in 'dsl', and add them to an AsyncGroup.
	// Limit concurrency in the AsyncGroup.
	var backupJobs []jobs.Job
	for _, ds := range dsl {
		backupJobs = append(backupJobs, m.WithQueue(
			m.withMetadata(
				h.BackupJob(rctx, backup, ds),
				backup,
				ds,
			),
		))
	}

	backupJob := m.wrapWithCommands(
		rctx,
		m.prepareBackupJob(rctx, backup),
		jobs.WithTimeout(jobs.AsyncGroup(backupJobs), src.Timeout),
		src.PreBackupCommand,
		src.PostBackupCommand,
	)

	// Group the jobs (sequentially) if there's more than one of
	// them. Give the final job a status and a user-visible name,
	// for debugging purposes. Set an exclusive lock with a
	// leave-running policy, so no more than one backup per
	// datasource can run at any given time. Finally, the job runs
	// in the 'backup' queue for concurrency limiting.
	//
	// Oh, and here is where we add per-dataset instrumentation.
	id := fmt.Sprintf("backup-source-%s", src.Name)
	return m.WithStatus(
		m.WithExclusiveLock(
			m.withWorkDir(
				withInstrumentation(
					JobWithLogPrefix(
						backupJob,
						fmt.Sprintf("backup=%s", backup.ID),
					),
					src.Name,
				),
			),
			id,
			false),
		id,
	), nil
}

func (m *tabaccoManager) BackupJob(ctx context.Context, src *SourceSpec) (*Backup, jobs.Job, error) {
	// Create a new Backup.
	b := newBackup("")

	// Create a RuntimeContext.
	rctx := m.configMgr.NewRuntimeContext()

	j, err := m.makeBackupJob(ctx, rctx, b, src)
	return b, j, err
}

// Backup just runs the BackupJob synchronously.
func (m *tabaccoManager) Backup(ctx context.Context, src *SourceSpec) (*Backup, error) {
	backup, job, err := m.BackupJob(ctx, src)
	if err != nil {
		return backup, err
	}
	err = job.RunContext(ctx)
	return backup, err
}

func (m *tabaccoManager) makeRestoreJob(rctx RuntimeContext, backup *Backup, src *SourceSpec, dsl []*Dataset, target string) (jobs.Job, error) {
	// Just need the Handler.
	hspec := rctx.HandlerSpec(src.Handler)
	if hspec == nil {
		return nil, fmt.Errorf("unknown handler '%s'", src.Handler)
	}
	h, err := hspec.Parse(src)
	if err != nil {
		return nil, err
	}

	// The actual backup operation. Just a thin wrapper around
	// doBackupDataset() that binds together the context, backup,
	// ds and target via the closure.
	var restoreJobs []jobs.Job
	for _, ds := range dsl {
		restoreJobs = append(
			restoreJobs,
			m.WithQueue(
				h.RestoreJob(rctx, backup, ds, target),
			),
		)
	}

	restoreJob := m.wrapWithCommands(
		rctx,
		nil,
		jobs.AsyncGroup(restoreJobs),
		src.PreRestoreCommand,
		src.PostRestoreCommand,
	)

	// Group the jobs (sequentially) if there's more than one of
	// them. Give the final job a status and a user-visible name,
	// for debugging purposes. Set an exclusive lock with a
	// leave-running policy, so no more than one restore per
	// datasource can run at any given time. Finally, the job runs
	// in the 'restore' queue for concurrency limiting.
	id := fmt.Sprintf("restore-source-%s", src.Name)
	return m.WithStatus(
		m.WithExclusiveLock(
			JobWithLogPrefix(
				restoreJob,
				fmt.Sprintf("restore=%s", backup.ID),
			),
			id,
			false),
		id,
	), nil
}

func groupDatasetsBySource(dsl []*Dataset) map[string][]*Dataset {
	m := make(map[string][]*Dataset)
	for _, ds := range dsl {
		m[ds.Source] = append(m[ds.Source], ds)
	}
	return m
}

// RestoreJob creates a job that restores the results of the
// FindRequest (with NumVersions=1) onto the given target directory.
func (m *tabaccoManager) RestoreJob(ctx context.Context, req *FindRequest, target string) (jobs.Job, error) {
	// Find the atoms relevant to this restore. The results will
	// be grouped in Backups and Datasets that only include the
	// relevant Atoms.
	req.NumVersions = 1
	backups, err := m.ms.FindAtoms(ctx, req)
	if err != nil {
		return nil, err
	}
	if len(backups) == 0 {
		return nil, errors.New("no results found for query")
	}

	// Create a RuntimeContext.
	rctx := m.configMgr.NewRuntimeContext()

	var restoreJobs []jobs.Job
	merr := new(util.MultiError)
	for _, b := range backups {
		// Group the datasets by source, find the source and create the restore jobs.
		for srcName, dsl := range groupDatasetsBySource(b.Datasets) {

			src := rctx.FindSource(srcName)
			if src == nil {
				merr.Add(fmt.Errorf("unknown source '%s'", srcName))
				continue
			}

			j, err := m.makeRestoreJob(rctx, b, src, dsl, target)
			if err != nil {
				merr.Add(fmt.Errorf("source %s: %v", srcName, err))
				continue
			}
			restoreJobs = append(restoreJobs, j)
		}
	}

	return m.WithStatus(jobs.AsyncGroup(restoreJobs), fmt.Sprintf("restore_%s", util.RandomID())), merr.OrNil()
}

// Restore just runs the RestoreJob synchronously.
func (m *tabaccoManager) Restore(ctx context.Context, req *FindRequest, target string) error {
	job, err := m.RestoreJob(ctx, req, target)
	if err != nil {
		return err
	}
	return job.RunContext(ctx)
}

// Create a new Backup object with its own unique ID (which actually
// consists of 16 random bytes, hex-encoded).
func newBackup(host string) *Backup {
	if host == "" {
		host = hostname // nolint
	}
	return &Backup{
		ID:        util.RandomID(),
		Host:      host,
		Timestamp: time.Now(),
	}
}

func (m *tabaccoManager) commandJob(rctx RuntimeContext, cmd string) jobs.Job {
	return jobs.JobFunc(func(ctx context.Context) error {
		return rctx.Shell().Run(ctx, cmd)
	})
}

// Global short hostname.
var hostname string

func init() {
	if s, _ := os.Hostname(); s != "" {
		if n := strings.IndexByte(s, '.'); n > 0 {
			s = s[:n]
		}
		hostname = s
	}
}
