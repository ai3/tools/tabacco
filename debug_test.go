package tabacco

import (
	"context"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"path/filepath"
	"regexp"
	"testing"
	"time"

	"git.autistici.org/ai3/tools/tabacco/jobs"
)

func runDebugTest(t *testing.T, tmpdir string, source *SourceSpec, testFn func(string)) {
	// Temporary cache dir.
	cacheDir, err := ioutil.TempDir("", "")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(cacheDir)

	store := &dummyMetadataStore{}

	repoSpec := RepositorySpec{
		Name: "main",
		Type: "restic",
		Params: map[string]interface{}{
			"uri":       tmpdir + "/repo",
			"password":  "testpass",
			"cache_dir": cacheDir,
		},
	}
	handlerSpecs := []*HandlerSpec{}
	queueSpec := &jobs.QueueSpec{
		Concurrency: 2,
	}
	sourceSpecs := []*SourceSpec{source}

	// Run the backup.
	configMgr, err := NewConfigManager(&Config{
		Queue:        queueSpec,
		Repository:   repoSpec,
		HandlerSpecs: handlerSpecs,
		SourceSpecs:  sourceSpecs,
	})
	if err != nil {
		t.Fatalf("NewConfigManager: %v", err)
	}

	agent, err := NewAgent(context.Background(), configMgr, store)
	if err != nil {
		t.Fatalf("NewAgent: %v", err)
	}
	defer agent.Close()

	srv := httptest.NewServer(agent.Handler())
	defer srv.Close()

	// Run our test function concurrently to the backup.
	doneCh := make(chan error)
	go func() {
		_, err := agent.mgr.Backup(context.TODO(), configMgr.current().SourceSpecs()[0])
		doneCh <- err
	}()

	ticker := time.NewTicker(100 * time.Millisecond)
	defer ticker.Stop()
outer:
	for {
		select {
		case <-ticker.C:
			testFn(srv.URL)
		case err := <-doneCh:
			if err != nil {
				t.Fatal(err)
			}
			break outer
		}
	}
}

func TestDebug_Active(t *testing.T) {
	tmpdir := createTempDirWithData(t)
	defer os.RemoveAll(tmpdir)
	client := new(http.Client)
	var maxActiveJobs int
	runDebugTest(
		t, tmpdir,
		&SourceSpec{
			Name:     "source1",
			Handler:  "file",
			Schedule: "@random_every 1h",
			Params: map[string]interface{}{
				"path": filepath.Join(tmpdir, "data"),
			},
			Datasets: []*DatasetSpec{
				&DatasetSpec{
					Atoms: []Atom{
						{
							Name: "f1",
							Path: "file1",
						},
						{
							Name: "f2",
							Path: "file2",
						},
					},
				},
			},
		},
		func(uri string) {
			resp, err := client.Get(uri + "/debug/restic")
			if err != nil {
				return
			}
			data, _ := ioutil.ReadAll(resp.Body)
			resp.Body.Close()
			if resp.StatusCode != 200 {
				t.Errorf("status page returned error %s", resp.Status)
				return
			}
			n := parseActiveJobs(data)
			log.Printf("debug request to %s: %d active jobs", uri, n)
			if n > maxActiveJobs {
				maxActiveJobs = n
			}
		},
	)

	if maxActiveJobs != 1 {
		t.Fatalf("found %d max active jobs, expected 1", maxActiveJobs)
	}
}

var activeJobRowRx = regexp.MustCompile(`<tr>\s+<td>`)

func parseActiveJobs(data []byte) int {
	matches := activeJobRowRx.FindAll(data, -1)
	return len(matches)
}
