package main

import (
	"context"
	"flag"
	"log"
	"os"

	"git.autistici.org/ai3/go-common/serverutil"
	mdbs "git.autistici.org/ai3/tools/tabacco/metadb/server"
	"github.com/google/subcommands"
	"gopkg.in/yaml.v3"
)

type metadbCommand struct {
	configPath string
	addr       string
}

func (c *metadbCommand) Name() string     { return "metadb" }
func (c *metadbCommand) Synopsis() string { return "start the metadata database server" }
func (c *metadbCommand) Usage() string {
	return `metadb [<flags>]:
	Start the metadata database server.

`
}

func (c *metadbCommand) SetFlags(f *flag.FlagSet) {
	f.StringVar(&c.configPath, "config", "/etc/tabacco/metadb.yml", "configuration `file`")
	f.StringVar(&c.addr, "addr", ":5332", "listen `address` for the HTTP server")
}

type metadbConfig struct {
	Driver       string                   `yaml:"db_driver"`
	DBURI        string                   `yaml:"db_uri"`
	ServerConfig *serverutil.ServerConfig `yaml:"http_server"`
	MaxAgeDays   int                      `yaml:"max_metadata_age_days"`
}

func loadMetadbConfig(path string) (*metadbConfig, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	config := metadbConfig{
		Driver:     "sqlite3",
		DBURI:      "/var/lib/tabacco-metadb/meta.db",
		MaxAgeDays: 180,
	}
	err = yaml.NewDecoder(f).Decode(&config)
	return &config, err
}

func (c *metadbCommand) Execute(ctx context.Context, f *flag.FlagSet, args ...interface{}) subcommands.ExitStatus {
	if f.NArg() > 0 {
		log.Printf("error: too many arguments")
		return subcommands.ExitUsageError
	}

	config, err := loadMetadbConfig(c.configPath)
	if err != nil {
		log.Printf("configuration error: %v", err)
		return subcommands.ExitFailure
	}

	svc, err := mdbs.New(config.Driver, config.DBURI, config.MaxAgeDays)
	if err != nil {
		log.Printf("error: %v", err)
		return subcommands.ExitFailure
	}
	if err := mdbs.Serve(svc, config.ServerConfig, c.addr); err != nil {
		log.Printf("error: %v", err)
		return subcommands.ExitFailure
	}

	return subcommands.ExitSuccess
}

func init() {
	subcommands.Register(&metadbCommand{}, "")
}
