package main

import (
	"context"
	"flag"
	"log"
	"time"

	"github.com/google/subcommands"

	"git.autistici.org/ai3/tools/tabacco"
	"git.autistici.org/ai3/tools/tabacco/jobs"
	mdbc "git.autistici.org/ai3/tools/tabacco/metadb/client"
)

type restoreCommand struct {
	configPath string
	httpAddr   string
	targetDir  string
	olderThan  timeFlag
}

func (c *restoreCommand) Name() string     { return "restore" }
func (c *restoreCommand) Synopsis() string { return "restore one or more datasets" }
func (c *restoreCommand) Usage() string {
	return `restore [<flags>] <dataset_filter(s)>...
        Restore datasets to the local host, based on the provided
        dataset matching criteria.

`
}

func (c *restoreCommand) SetFlags(f *flag.FlagSet) {
	f.StringVar(&c.configPath, "config", "/etc/tabacco/agent.yml", "configuration `file`")
	f.StringVar(&c.httpAddr, "http-addr", ":5330", "listen `address` for the HTTP server exporting metrics and debugging")
	f.StringVar(&c.targetDir, "target", ".", "target `path` for restore")
	f.Var(&c.olderThan, "older-than", "pick the first backup older than `TIME`")
}

func (c *restoreCommand) Execute(ctx context.Context, f *flag.FlagSet, args ...interface{}) subcommands.ExitStatus {
	if f.NArg() == 0 {
		log.Printf("error: not enough arguments")
		return subcommands.ExitUsageError
	}

	configMgr, mgr, err := c.initManager(ctx)
	if err != nil {
		log.Printf("error: %v", err)
		return subcommands.ExitFailure
	}
	defer configMgr.Close()
	defer mgr.Close()

	// Build the tree of restore jobs and execute it.
	j, err := c.buildRestoreJob(ctx, mgr, f.Args())
	if err != nil {
		log.Printf("error: %v", err)
		return subcommands.ExitFailure
	}
	if err := j.RunContext(ctx); err != nil {
		log.Printf("error: %v", err)
		return subcommands.ExitFailure
	}

	return subcommands.ExitSuccess
}

func (c *restoreCommand) buildRestoreJob(ctx context.Context, mgr tabacco.Manager, args []string) (jobs.Job, error) {
	var restoreJobs []jobs.Job

	for _, arg := range args {
		j, err := mgr.RestoreJob(ctx, c.newFindRequest(arg), c.targetDir)
		if err != nil {
			return nil, err
		}
		log.Printf("preparing restore of %s", arg)
		restoreJobs = append(restoreJobs, j)
	}

	return jobs.AsyncGroup(restoreJobs), nil
}

func (c *restoreCommand) newFindRequest(s string) *tabacco.FindRequest {
	return &tabacco.FindRequest{
		Pattern:   s,
		OlderThan: time.Time(c.olderThan),
	}
}

func (c *restoreCommand) initManager(ctx context.Context) (*tabacco.ConfigManager, tabacco.Manager, error) {
	// Build a ConfigManager.
	config, err := tabacco.ReadConfig(c.configPath)
	if err != nil {
		return nil, nil, err
	}
	configMgr, err := tabacco.NewConfigManager(config)
	if err != nil {
		return nil, nil, err
	}

	// Connect to the metadata store.
	store, err := mdbc.New(config.MetadataStoreBackend)
	if err != nil {
		return nil, nil, err
	}

	// Directly create a Manager and tell it to invoke a restore.
	mgr, err := tabacco.NewManager(ctx, configMgr, store)
	if err != nil {
		return nil, nil, err
	}

	return configMgr, mgr, nil
}

func init() {
	subcommands.Register(&withSignalHandlers{&restoreCommand{}}, "")
}
