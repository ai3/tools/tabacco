package main

import (
	"context"
	"flag"
	"log"
	"os"
	"os/signal"
	"syscall"

	"git.autistici.org/ai3/tools/tabacco"
	mdbc "git.autistici.org/ai3/tools/tabacco/metadb/client"
	"github.com/google/subcommands"
)

type agentCommand struct {
	configPath string
	httpAddr   string
}

func (c *agentCommand) Name() string     { return "agent" }
func (c *agentCommand) Synopsis() string { return "start the backup agent" }
func (c *agentCommand) Usage() string {
	return `agent [<flags>]:
	Start the backup agent.

`
}

func (c *agentCommand) SetFlags(f *flag.FlagSet) {
	f.StringVar(&c.configPath, "config", "/etc/tabacco/agent.yml", "configuration `file`")
	f.StringVar(&c.httpAddr, "http-addr", ":5331", "listen `address` for the HTTP server exporting metrics and debugging")
}

func (c *agentCommand) Execute(ctx context.Context, f *flag.FlagSet, args ...interface{}) subcommands.ExitStatus {
	if f.NArg() > 0 {
		log.Printf("error: too many arguments")
		return subcommands.ExitUsageError
	}

	// Build a ConfigManager that can be reloaded with SIGHUP.
	config, err := tabacco.ReadConfig(c.configPath)
	if err != nil {
		log.Printf("configuration error: %v", err)
		return subcommands.ExitFailure
	}
	configMgr, err := tabacco.NewConfigManager(config)
	if err != nil {
		log.Printf("configuration error: %v", err)
		return subcommands.ExitFailure
	}
	defer configMgr.Close()
	hupCh := make(chan os.Signal, 1)
	go func() {
		for range hupCh {
			log.Printf("SIGHUP received, reloading configuration")
			newConfig, err := tabacco.ReadConfig(c.configPath) // nolint: vetshadow
			if err != nil {
				log.Printf("configuration error: %v", err)
				continue
			}
			if err := configMgr.Reload(newConfig); err != nil {
				log.Printf("error reloading configuration: %v", err)
			}
		}
	}()
	signal.Notify(hupCh, syscall.SIGHUP)

	store, err := mdbc.New(config.MetadataStoreBackend)
	if err != nil {
		log.Printf("error: %v", err)
		return subcommands.ExitFailure
	}

	// Build the Agent, and hook SIGUSR1 so that it triggers all
	// the backup jobs immediately (useful for emergencies or
	// debugging purposes).
	agent, err := tabacco.NewAgent(ctx, configMgr, store)
	if err != nil {
		log.Printf("error: %v", err)
		return subcommands.ExitFailure
	}
	defer agent.Close() // nolint
	usr1Ch := make(chan os.Signal, 1)
	go func() {
		for range usr1Ch {
			log.Printf("SIGUSR1 received, starting all jobs immediately")
			agent.RunNow()
		}
	}()
	signal.Notify(usr1Ch, syscall.SIGUSR1)

	log.Printf("backup agent started")

	if c.httpAddr != "" {
		agent.StartHTTPServer(c.httpAddr)
	}

	// Wait for the outmost Context to terminate (presumably due to SIGTERM).
	<-ctx.Done()

	log.Printf("backup agent stopped")

	return subcommands.ExitSuccess
}

func init() {
	subcommands.Register(&withSignalHandlers{&agentCommand{}}, "")
}
