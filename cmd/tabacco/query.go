package main

import (
	"context"
	"encoding/json"
	"errors"
	"flag"
	"log"
	"os"
	"time"

	"github.com/google/subcommands"

	"git.autistici.org/ai3/tools/tabacco"
	mdbc "git.autistici.org/ai3/tools/tabacco/metadb/client"
)

var rpcTimeout = 1800 * time.Second

type timeFlag time.Time

func (f timeFlag) String() string {
	return time.Time(f).Format(time.RFC3339)
}

func (f *timeFlag) Set(value string) error {
	for _, fmt := range []string{
		time.RFC3339,
		time.Stamp,
		time.Kitchen,
		"2006/01/02",
		"2006-01-02",
	} {
		if t, err := time.Parse(fmt, value); err == nil {
			log.Printf("parsed %s as %v", value, t)
			*f = timeFlag(t)
			return nil
		}
	}
	return errors.New("could not parse time value")
}

type queryCommand struct {
	configPath  string
	host        string
	numVersions int
	olderThan   timeFlag
}

func (c *queryCommand) Name() string     { return "query" }
func (c *queryCommand) Synopsis() string { return "query the backup metadata database" }
func (c *queryCommand) Usage() string {
	return `query [<flags>] <atom_pattern>
        Query the backup metadata database.

`
}

func (c *queryCommand) SetFlags(f *flag.FlagSet) {
	f.StringVar(&c.configPath, "config", "/etc/tabacco/agent.yml", "configuration `file`")
	f.StringVar(&c.host, "host", "", "filter by host")
	f.IntVar(&c.numVersions, "num-versions", 1, "return the most recent `N` versions")
	f.Var(&c.olderThan, "older-than", "return a backup older than `TIME`")
}

func (c *queryCommand) buildRequest(f *flag.FlagSet) (*tabacco.FindRequest, error) {
	if f.NArg() != 1 {
		return nil, errors.New("error: wrong number of arguments")
	}
	return &tabacco.FindRequest{
		Pattern:     f.Arg(0),
		Host:        c.host,
		NumVersions: c.numVersions,
		OlderThan:   time.Time(c.olderThan),
	}, nil
}

func (c *queryCommand) Execute(ctx context.Context, f *flag.FlagSet, args ...interface{}) subcommands.ExitStatus {
	req, err := c.buildRequest(f)
	if err != nil {
		log.Printf("error in request: %v", err)
		return subcommands.ExitUsageError
	}

	// Parse configuration and connect to the metadata store.
	config, err := tabacco.ReadConfig(c.configPath)
	if err != nil {
		log.Printf("error reading config: %v", err)
		return subcommands.ExitFailure
	}
	store, err := mdbc.New(config.MetadataStoreBackend)
	if err != nil {
		log.Printf("error in metadata client config: %v", err)
		return subcommands.ExitFailure
	}

	// Make the RPC.
	rctx, cancel := context.WithTimeout(ctx, rpcTimeout)
	result, err := store.FindAtoms(rctx, req)
	cancel()

	if err != nil {
		log.Printf("FindAtoms() error: %v", err)
		return subcommands.ExitFailure
	}

	data, _ := json.MarshalIndent(result, "", "  ")
	os.Stdout.Write(data)

	return subcommands.ExitSuccess
}

func init() {
	subcommands.Register(&queryCommand{}, "")
}
