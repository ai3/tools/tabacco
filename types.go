package tabacco

import (
	"context"
	"fmt"
	"regexp"
	"strings"
	"time"

	"git.autistici.org/ai3/tools/tabacco/jobs"
)

// Params are configurable parameters in a format friendly to YAML
// representation.
type Params map[string]interface{}

// Get a string value for a parameter.
func (p Params) Get(key string) string {
	if s, ok := p[key].(string); ok {
		return s
	}
	return ""
}

// GetList returns a string list value for a parameter.
func (p Params) GetList(key string) []string {
	if l, ok := p[key].([]string); ok {
		return l
	}
	return nil
}

// GetBool returns a boolean value for a parameter (may be a string).
// Returns value and presence.
func (p Params) GetBool(key string) (bool, bool) {
	if b, ok := p[key].(bool); ok {
		return b, true
	}
	if s, ok := p[key].(string); ok {
		switch strings.ToLower(s) {
		case "on", "yes", "true", "1":
			return true, true
		}
		return false, true
	}
	return false, false
}

// Backup is the over-arching entity describing a high level backup
// operation. Backups are initiated autonomously by individual hosts,
// so each Backup belongs to a single Host.
type Backup struct {
	// Unique identifier.
	ID string `json:"id"`

	// Timestamp (backup start).
	Timestamp time.Time `json:"timestamp"`

	// Host.
	Host string `json:"host"`

	// Datasets.
	Datasets []*Dataset `json:"datasets"`
}

// An Atom is a bit of data that can be restored independently as part
// of a Dataset. Atoms are identified uniquely by their absolute path
// in the global atom namespace: this path is built by concatenating
// the source name, the dataset name, and the atom name.
type Atom struct {
	// Name (path-like).
	Name string `json:"name"`

	// Special attribute for the 'file' handler (path relative to
	// source root path).
	Path string `json:"path,omitempty"`
}

// A Dataset describes a data set as a high level structure containing
// one or more atoms. The 1-to-many scenario is justified by the
// following use case: imagine a sql database server, we may want to
// back it up as a single operation, but it contains multiple
// databases (the atom we're interested in), which we might want to
// restore independently.
type Dataset struct {
	// Unique identifier.
	ID string `json:"id"`

	// Source is the name of the source that created this Dataset,
	// stored so that the restore knows what to do.
	Source string `json:"source"`

	// Atoms that are part of this dataset.
	Atoms []Atom `json:"atoms"`

	// Snapshot ID (repository-specific).
	SnapshotID string `json:"snapshot_id"`

	// Number of files in this dataset.
	TotalFiles int64 `json:"total_files"`

	// Number of bytes in this dataset.
	TotalBytes int64 `json:"total_bytes"`

	// Number of bytes that were added / removed in this backup.
	BytesAdded int64 `json:"bytes_added"`

	// Duration in seconds.
	Duration int `json:"duration"`
}

// FindRequest specifies search criteria for atoms.
type FindRequest struct {
	Pattern   string `json:"pattern"`
	patternRx *regexp.Regexp

	Host        string    `json:"host"`
	NumVersions int       `json:"num_versions"`
	OlderThan   time.Time `json:"older_than,omitempty"`
}

func (req *FindRequest) matchPattern(s string) bool {
	if req.patternRx == nil {
		req.patternRx = regexp.MustCompile(
			fmt.Sprintf("^%s$", strings.Replace(req.Pattern, "*", ".*", -1)))
	}
	return req.patternRx.MatchString(s)
}

// MetadataStore is the client interface to the global metadata store.
type MetadataStore interface {
	// Find the datasets that match a specific criteria. Only
	// atoms matching the criteria will be included in the Dataset
	// objects in the response.
	FindAtoms(context.Context, *FindRequest) ([]*Backup, error)

	// Add a dataset entry (the Backup might already exist).
	AddDataset(context.Context, *Backup, *Dataset) error

	// StartUpdates spawns a goroutine that periodically sends
	// active job status updates to the metadata server.
	StartUpdates(context.Context, func() *UpdateActiveJobStatusRequest)
}

// Handler can backup and restore a specific class of datasets.
type Handler interface {
	BackupJob(RuntimeContext, *Backup, *Dataset) jobs.Job
	RestoreJob(RuntimeContext, *Backup, *Dataset, string) jobs.Job
}

// Repository is the interface to a remote repository.
type Repository interface {
	Init(context.Context, RuntimeContext) error

	RunBackup(context.Context, *Shell, *Backup, *Dataset, string, []string) error
	RunStreamBackup(context.Context, *Shell, *Backup, *Dataset, string, string) error

	RunRestore(context.Context, *Shell, *Backup, *Dataset, []string, string) error
	RunStreamRestore(context.Context, *Shell, *Backup, *Dataset, string, string) error

	Close() error
}

// Manager for backups and restores.
type Manager interface {
	BackupJob(context.Context, *SourceSpec) (*Backup, jobs.Job, error)
	Backup(context.Context, *SourceSpec) (*Backup, error)
	RestoreJob(context.Context, *FindRequest, string) (jobs.Job, error)
	Restore(context.Context, *FindRequest, string) error
	Close() error

	// Debug interface.
	GetStatus() ([]jobs.Status, []jobs.Status, []jobs.Status)
}

// RunningJobStatus has information about a backup job that is currently running.
type RunningJobStatus resticStatusMessage

// JobStatus has contextual information about a backup job that is currently running.
type JobStatus struct {
	Host          string            `json:"host"`
	JobID         string            `json:"job_id"`
	BackupID      string            `json:"backup_id"`
	DatasetID     string            `json:"dataset_id"`
	DatasetSource string            `json:"dataset_source"`
	Status        *RunningJobStatus `json:"status"`
}

// UpdateActiveJobStatusRequest is the periodic "ping" sent by agents
// (unique host names are assumed) containing information about
// currently running jobs.
type UpdateActiveJobStatusRequest struct {
	Host       string       `json:"host"`
	ActiveJobs []*JobStatus `json:"active_jobs,omitempty"`
}
