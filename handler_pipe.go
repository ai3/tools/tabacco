package tabacco

import (
	"context"
	"errors"
	"os"
	"strings"

	"git.autistici.org/ai3/tools/tabacco/jobs"
)

// The pipeHandler must work on a 1:1 dataset/atom mapping, because it
// generates a single file on the repository, and thus it can't
// distinguish multiple atoms inside it.
type pipeHandler struct {
	backupCmd     string
	restoreCmd    string
	compress      bool
	compressCmd   string
	decompressCmd string
}

const (
	defaultCompress      = false
	defaultCompressCmd   = "zstd -c --rsyncable"
	defaultDecompressCmd = "zstd -dcf"
)

func newPipeHandler(name string, params Params) (Handler, error) {
	backupCmd := params.Get("backup_command")
	if backupCmd == "" {
		return nil, errors.New("backup_command not set")
	}

	restoreCmd := params.Get("restore_command")
	if restoreCmd == "" {
		return nil, errors.New("restore_command not set")
	}

	// Create the pipeHandler with defaults, which can be
	// overriden from Params.
	h := &pipeHandler{
		backupCmd:     backupCmd,
		restoreCmd:    restoreCmd,
		compress:      defaultCompress,
		compressCmd:   defaultCompressCmd,
		decompressCmd: defaultDecompressCmd,
	}
	if b, ok := params.GetBool("compress"); ok {
		h.compress = b
	}
	if s := params.Get("compress_command"); s != "" {
		h.compressCmd = s
	}
	if s := params.Get("decompress_command"); s != "" {
		h.decompressCmd = s
	}

	return h, nil
}

func (h *pipeHandler) BackupJob(rctx RuntimeContext, backup *Backup, ds *Dataset) jobs.Job {
	return jobs.JobFunc(func(ctx context.Context) error {
		return rctx.Repo().RunStreamBackup(ctx, rctx.Shell(), backup, ds, h.backupCmd, h.compressCommand())
	})
}

func (h *pipeHandler) RestoreJob(rctx RuntimeContext, backup *Backup, ds *Dataset, target string) jobs.Job {
	return jobs.JobFunc(func(ctx context.Context) error {
		return rctx.Repo().RunStreamRestore(ctx, rctx.Shell(), backup, ds, h.restoreCmd, h.decompressCommand())
	})
}

func (h *pipeHandler) compressCommand() string {
	if !h.compress {
		return ""
	}
	return h.compressCmd
}

func (h *pipeHandler) decompressCommand() string {
	if !h.compress {
		return ""
	}
	return h.decompressCmd
}

func expandVars(s string, backup *Backup, ds *Dataset) string {
	return os.Expand(s, func(key string) string {
		switch key {
		case "$":
			return key
		case "backup.id":
			return backup.ID
		case "atom.names":
			names := make([]string, 0, len(ds.Atoms))
			for _, a := range ds.Atoms {
				names = append(names, a.Name)
			}
			return strings.Join(names, " ")
		case "atom.paths":
			paths := make([]string, 0, len(ds.Atoms))
			for _, a := range ds.Atoms {
				paths = append(paths, a.Path)
			}
			return strings.Join(paths, " ")
		default:
			return os.Getenv(key)
		}
	})
}
