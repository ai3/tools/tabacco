package util

import (
	"strings"
)

// MultiError is just a container of other errors, wrapped into a
// single error.
type MultiError struct {
	errors []error
}

// Add an error to the container.
func (m *MultiError) Add(err error) {
	m.errors = append(m.errors, err)
}

// Error returns the error string. Implements the 'error' interface.
func (m *MultiError) Error() string {
	var tmp []string
	for _, e := range m.errors {
		tmp = append(tmp, e.Error())
	}
	return strings.Join(tmp, ", ")
}

// Errors returns a list of all errors in the container.
func (m *MultiError) Errors() []error {
	return m.errors
}

// OrNil returns either this same error, if not empty, or nil.
func (m *MultiError) OrNil() error {
	if len(m.errors) > 0 {
		return m
	}
	return nil
}

// IsNil returns true if the container is empty.
func (m *MultiError) IsNil() bool {
	return len(m.errors) == 0
}
