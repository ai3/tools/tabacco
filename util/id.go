package util

import (
	crand "crypto/rand"
	"encoding/binary"
	"encoding/hex"
	"math/rand"
)

var rndID *rand.Rand

func init() {
	seed, _ := RandomSeed()
	rndID = rand.New(rand.NewSource(seed))
}

// RandomID generates a random unique ID. It will return an identifier
// consisting of 32 ascii-friendly bytes (16 random bytes,
// hex-encoded).
func RandomID() string {
	var b [16]byte
	binary.LittleEndian.PutUint64(b[:8], rndID.Uint64())
	binary.LittleEndian.PutUint64(b[8:16], rndID.Uint64())
	return hex.EncodeToString(b[:])
}

// Generate a random int64, and return it along with its byte
// representation (encoding/binary, little-endian).
func RandomSeed() (int64, []byte) {
	// Initialize the seed from a secure source.
	var b [8]byte
	if _, err := crand.Read(b[:]); err != nil { // nolint: gosec
		panic(err)
	}
	seed := binary.LittleEndian.Uint64(b[:])
	return int64(seed), b[:]
}
