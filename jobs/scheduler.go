package jobs

import (
	"context"
	"hash/crc64"
	"log"
	"math/rand"
	"sort"
	"strings"
	"sync"
	"time"

	"git.autistici.org/ai3/tools/tabacco/util"
	cron "github.com/robfig/cron/v3"
)

// JobGeneratorFunc is a function that returns a new Job.
type JobGeneratorFunc func() Job

// Exit status of a named cron job.
type cronJobExitStatus struct {
	name string
	err  error
}

// A Schedule configures a Scheduler with job generators.
type Schedule struct {
	hostSeed int64
	rootCtx  context.Context

	// Runtime components. The Schedule actually runs the cron
	// jobs, the Scheduler is just a switching wrapper to an
	// active Schedule. But the Scheduler maintains a persistent
	// (across reloads) log of the last error / success for every
	// job, so we use a channel to link the two.
	c        *cron.Cron
	mx       sync.Mutex
	notifyCh chan cronJobExitStatus
}

// NewSchedule creates a new Schedule. The context passed to this
// function is the one that all scheduled jobs will be using, so use
// it for global cancellation, or just pass context.Background().
func NewSchedule(ctx context.Context, hostSeed int64) *Schedule {
	return &Schedule{
		hostSeed: hostSeed,
		rootCtx:  ctx,
		c:        cron.New(),
	}
}

// Add a task to the schedule.
func (s *Schedule) Add(name, schedStr string, jobFn JobGeneratorFunc) error {
	sched, err := parseSchedule(schedStr, s.hostSeed)
	if err != nil {
		return err
	}
	s.c.Schedule(sched, &cronJob{
		name:        name,
		fn:          jobFn,
		ctx:         s.rootCtx,
		schedule:    s,
		scheduleStr: schedStr,
	})
	return nil
}

func (s *Schedule) notify(name string, err error) {
	s.mx.Lock()
	if s.notifyCh != nil {
		s.notifyCh <- cronJobExitStatus{name: name, err: err}
	}
	s.mx.Unlock()
}

func (s *Schedule) start(notifyCh chan cronJobExitStatus) {
	s.mx.Lock()
	s.notifyCh = notifyCh
	s.mx.Unlock()
	s.c.Start()
}

func (s *Schedule) stop() {
	s.c.Stop()
	s.mx.Lock()
	s.notifyCh = nil
	s.mx.Unlock()
}

// A cronJob implements cron.Job. It will generate a new Job and start
// it in a new goroutine, so, from the point of view of the cron
// package, cron jobs are instantaneous.
type cronJob struct {
	name        string
	scheduleStr string
	fn          JobGeneratorFunc
	ctx         context.Context

	// Link back to the Schedule so we can notify the Scheduler
	// about exit status.
	schedule *Schedule
}

func (j *cronJob) Name() string { return j.name }

func (j *cronJob) Schedule() string { return j.scheduleStr }

func (j *cronJob) Run() {
	job := j.fn()
	if job == nil {
		return
	}

	go func() {
		log.Printf("scheduled job %s starting", j.name)
		err := job.RunContext(j.ctx)
		if err != nil {
			log.Printf("scheduled job %s failed: %v", j.name, err)
		} else {
			log.Printf("scheduled job %s succeeded", j.name)
		}
		j.schedule.notify(j.name, err)
	}()
}

// A Scheduler triggers Jobs on a periodic schedule. It uses job
// generators (functions that return Jobs) to create new jobs at the
// desired time.
//
// The standard cron syntax (documentation available at
// https://github.com/robfig/cron) is extended with the syntax:
//
//     @random_every <duration>
//
// where 'duration' specifies a time.Duration as recognized by
// time.ParseDuration. This results in the job running at a time with
// a random offset within the given period. The offset stays constant
// over time because the random seed it's generated from is saved in a
// file.
type Scheduler struct {
	mx  sync.Mutex
	cur *Schedule

	notifyMx  sync.Mutex
	lastError map[string]error
	notifyCh  chan cronJobExitStatus
}

// NewScheduler creates a new Scheduler.
func NewScheduler() *Scheduler {
	s := &Scheduler{
		lastError: make(map[string]error),
		notifyCh:  make(chan cronJobExitStatus),
	}
	go func() {
		for ex := range s.notifyCh {
			s.notifyMx.Lock()
			s.lastError[ex.name] = ex.err
			s.notifyMx.Unlock()
		}
	}()
	return s
}

func (s *Scheduler) getLastErrorString(name string) string {
	s.notifyMx.Lock()
	defer s.notifyMx.Unlock()
	err := s.lastError[name]
	if err == nil {
		return ""
	}
	return err.Error()
}

// SetSchedule replaces the current schedule with a new one.
func (s *Scheduler) SetSchedule(schedule *Schedule) {
	s.mx.Lock()
	if s.cur != nil {
		s.cur.stop()
	}
	s.cur = schedule
	s.cur.start(s.notifyCh)
	s.mx.Unlock()
}

// Stop the scheduler (won't affect running jobs).
func (s *Scheduler) Stop() {
	s.mx.Lock()
	if s.cur != nil {
		s.cur.stop()
	}
	s.mx.Unlock()
	close(s.notifyCh)
}

// RunNow starts all jobs right now, regardless of their schedule.
func (s *Scheduler) RunNow() {
	s.mx.Lock()
	defer s.mx.Unlock()
	if s.cur == nil {
		return
	}

	for _, entry := range s.cur.c.Entries() {
		go func(entry cron.Entry) {
			entry.Job.Run()
		}(entry)
	}
}

// CronJobStatus represents the status of a job, either scheduled,
// running, or terminated in the past.
type CronJobStatus struct {
	Name      string
	Schedule  string
	Prev      time.Time
	Next      time.Time
	LastError string
}

type cronJobStatusList struct {
	list   []CronJobStatus
	lessFn func(int, int) bool
}

func (l *cronJobStatusList) Len() int           { return len(l.list) }
func (l *cronJobStatusList) Swap(i, j int)      { l.list[i], l.list[j] = l.list[j], l.list[i] }
func (l *cronJobStatusList) Less(i, j int) bool { return l.lessFn(i, j) }

func cronJobStatusListOrderByName(list []CronJobStatus) *cronJobStatusList {
	return &cronJobStatusList{
		list: list,
		lessFn: func(i, j int) bool {
			return list[i].Name < list[j].Name
		},
	}
}

type hasNameAndSchedule interface {
	Name() string
	Schedule() string
}

// GetStatus returns the current status of the scheduled jobs.
func (s *Scheduler) GetStatus() []CronJobStatus {
	s.mx.Lock()
	defer s.mx.Unlock()
	if s.cur == nil {
		return nil
	}

	var jobs []CronJobStatus
	for _, entry := range s.cur.c.Entries() {
		// Get the cronJob behind the cron.Job interface.
		if jj, ok := entry.Job.(hasNameAndSchedule); ok {
			name := jj.Name()
			jobs = append(jobs, CronJobStatus{
				Name:      name,
				Schedule:  jj.Schedule(),
				Prev:      entry.Prev,
				Next:      entry.Next,
				LastError: s.getLastErrorString(name),
			})
		}
	}

	// Sort everything.
	sort.Sort(cronJobStatusListOrderByName(jobs))

	return jobs
}

func parseSchedule(s string, hostSeed int64) (cron.Schedule, error) {
	switch {
	case strings.HasPrefix(s, "@random_every "):
		period, err := util.ParseDuration(s[14:])
		if err != nil {
			return nil, err
		}
		return randomScheduleEvery(makeScheduleRand(s, hostSeed), period), nil
	default:
		return cron.NewParser(
			cron.Minute | cron.Hour | cron.Dom | cron.Month | cron.Dow | cron.Descriptor).Parse(s)
	}
}

func randomScheduleEvery(rnd *rand.Rand, period time.Duration) cron.Schedule {
	// Find a random period. Base will be the offset
	offNS := rnd.Int63n(period.Nanoseconds())
	return &randomPeriodicSchedule{
		offset: offNS,
		period: period.Nanoseconds(),
	}
}

type randomPeriodicSchedule struct {
	offset int64
	period int64
}

func (s *randomPeriodicSchedule) Next(now time.Time) time.Time {
	// Perform the computation in nanoseconds, to take advantage
	// of integer division.
	nowNS := now.UnixNano()
	lastRounded := s.period * (nowNS / s.period)
	nextNS := lastRounded + s.offset
	if nextNS < nowNS {
		nextNS += s.period
	}
	next := time.Unix(0, nextNS)
	if next.Before(now) {
		panic("next before now!")
	}
	return next
}

var crc64Table *crc64.Table

func init() {
	crc64Table = crc64.MakeTable(crc64.ISO)
}

func makeScheduleRand(name string, hostSeed int64) *rand.Rand {
	// Mix the unique schedule name into the host seed.
	crc := int64(crc64.Checksum([]byte(name), crc64Table) & 0x7fffffff)
	seed := hostSeed ^ crc
	return rand.New(rand.NewSource(seed))
}
