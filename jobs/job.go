package jobs

import (
	"container/list"
	"context"
	"errors"
	"fmt"
	"log"
	"sync"
	"time"

	"git.autistici.org/ai3/tools/tabacco/util"
)

// Job is a task that can be run and canceled, and that has a string
// that can be used to identify successive instances of the same task,
// differing only in their execution time (so keys don't have to be
// unique). It's basically a glorified goroutine wrapper with a
// cancelable Context.
type Job interface {
	ID() string
	RunContext(context.Context) error
	Cancel()
	Wait() error
}

// Give a job its own random unique ID. The ID is stored in the
// Context and can be retrieved at runtime by calling the GetID()
// function.
type idJob struct {
	Job
	id string
}

type idJobKeyType int

var idJobKey idJobKeyType = 1

func (j *idJob) ID() string { return j.id }

func (j *idJob) RunContext(ctx context.Context) error {
	return j.Job.RunContext(context.WithValue(ctx, idJobKey, j.id))
}

// WithID gives a job a random unique ID.
func WithID(j Job) Job {
	return &idJob{
		Job: j,
		id:  util.RandomID(),
	}
}

// GetID returns the job ID when called from RunContext.
func GetID(ctx context.Context) string {
	id, ok := ctx.Value(idJobKey).(string)
	if ok {
		return id
	}
	return ""
}

// Adds a Cancel method to a job.
type cancelableJob struct {
	Job
	done     chan struct{}
	err      error
	cancelMx sync.Mutex
	cancel   context.CancelFunc
}

// WithCancel wraps a job with a Context that can be canceled by
// calling the Cancel() method. It also implements its own Wait()
// method, so you don't have to.
func WithCancel(j Job) Job {
	return &cancelableJob{
		Job:  j,
		done: make(chan struct{}),
	}
}

func (j *cancelableJob) RunContext(ctx context.Context) error {
	defer close(j.done)
	j.cancelMx.Lock()
	innerCtx, cancel := context.WithCancel(ctx)
	j.cancel = cancel
	defer cancel()
	j.cancelMx.Unlock()

	j.err = j.Job.RunContext(innerCtx)
	return j.err
}

func (j *cancelableJob) Cancel() {
	j.cancelMx.Lock()
	if j.cancel != nil {
		j.cancel()
	}
	j.cancelMx.Unlock()
}

func (j *cancelableJob) Wait() error {
	<-j.done
	return j.err
}

// A base job that is just a function. It can be canceled, and the
// result can be waited upon.
type funcJob struct {
	fn func(context.Context) error
}

// JobFunc creates a new cancelable Job that wraps a function call.
func JobFunc(fn func(context.Context) error) Job {
	return WithCancel(WithID(&funcJob{
		fn: fn,
	}))
}

func (j *funcJob) ID() string  { return "" }
func (j *funcJob) Cancel()     {}
func (j *funcJob) Wait() error { return errors.New("Wait not implemented") }

func (j *funcJob) RunContext(ctx context.Context) error {
	return j.fn(ctx)
}

// A job that runs with a timeout.
type timeoutJob struct {
	Job
	timeout time.Duration
}

// WithTimeout wraps a job with a timeout, after which the job is
// canceled.
func WithTimeout(j Job, timeout time.Duration) Job {
	return &timeoutJob{Job: j, timeout: timeout}
}

func (j *timeoutJob) RunContext(ctx context.Context) error {
	innerCtx, cancel := context.WithTimeout(ctx, j.timeout)
	defer cancel()
	return j.Job.RunContext(innerCtx)
}

// ExclusiveLockManager keeps an exclusive lock of running jobs by
// Key, and can apply an overrun policy to them: upon starting a new
// job, when a previous job with the same key exists, it can evict the
// previous job or abort starting the new one.
type ExclusiveLockManager struct {
	mx     sync.Mutex
	locked map[string]Job
}

// NewExclusiveLockManager returns a new ExclusiveLockManager.
func NewExclusiveLockManager() *ExclusiveLockManager {
	return &ExclusiveLockManager{
		locked: make(map[string]Job),
	}
}

// WithExclusiveLock wraps a Job with an exclusive lock, so that no
// more than one job with this key may be running at any given
// time. The killAndRun flag selects the desired overrun policy when a
// second task is started.
func (m *ExclusiveLockManager) WithExclusiveLock(j Job, lockKey string, killAndRun bool) Job {
	return &exclusiveJob{Job: j, mgr: m, lockKey: lockKey, killAndRun: killAndRun}
}

type exclusiveJob struct {
	Job
	mgr        *ExclusiveLockManager
	lockKey    string
	killAndRun bool
}

func (j *exclusiveJob) RunContext(ctx context.Context) error {
	key := j.lockKey

	j.mgr.mx.Lock()
	defer j.mgr.mx.Unlock()
	if cur, ok := j.mgr.locked[key]; ok {
		if !j.killAndRun {
			// TODO: return nil?
			return fmt.Errorf("job %s skipped (overrun)", j.ID())
		}
		cur.Cancel()
		cur.Wait() // nolint
	}
	j.mgr.locked[key] = j
	j.mgr.mx.Unlock()

	err := j.Job.RunContext(ctx)

	j.mgr.mx.Lock()
	delete(j.mgr.locked, key)
	return err
}

// QueueManager can limit the number of jobs by user-define tag. Each
// tag corresponds to a separate queue, limited to a certain number of
// parallel tasks. By default (or for unknown queues) there is no
// limit.
type QueueManager struct {
	sem chan struct{}
}

// QueueSpec describes the configuration of named queues.
type QueueSpec struct {
	Concurrency int `yaml:"concurrency"`
}

// NewQueueManager returns a new QueueManager with the provided
// configuration.
func NewQueueManager(spec *QueueSpec) *QueueManager {
	n := spec.Concurrency
	if n < 1 {
		n = 1
	}
	return &QueueManager{sem: make(chan struct{}, n)}
}

// WithQueue wraps a job with a concurrency limit controller.
func (m *QueueManager) WithQueue(j Job) Job {
	return &queuedJob{Job: j, sem: m.sem}
}

type queuedJob struct {
	Job
	sem chan struct{}
}

func (j *queuedJob) RunContext(ctx context.Context) error {
	select {
	case j.sem <- struct{}{}:
	case <-ctx.Done():
		return ctx.Err()
	}
	err := j.Job.RunContext(ctx)
	<-j.sem
	return err
}

// Enum for possible job states as tracked by the StateManager.
const (
	JobStatusPending = iota
	JobStatusRunning
	JobStatusDone
)

// JobStatus is an enum representing the state of a job.
type JobStatus int

// String returns a text representation of the job state.
func (s JobStatus) String() string {
	switch s {
	case JobStatusPending:
		return "PENDING"
	case JobStatusRunning:
		return "RUNNING"
	case JobStatusDone:
		return "DONE"
	default:
		return "UNKNOWN"
	}
}

// Status holds information on the current state of a job.
type Status struct {
	ID          string
	Name        string
	Status      JobStatus
	StartedAt   time.Time
	CompletedAt time.Time
	Err         error
	Job         Job
}

// StateManager adds a state and ID to jobs and keeps track of them
// after they have run. This is basically a way to keep track of
// running goroutines at the level of granularity that we desire.
//
// It has no practical effect on the jobs themselves, it's just a way
// to provide the user with debugging and auditing information.
type StateManager struct {
	mx      sync.Mutex
	pending map[string]*Status
	running map[string]*Status
	done    *list.List
}

// How many past executions to keep in memory.
const logsToKeep = 100

// NewStateManager creates a new StateManager.
func NewStateManager() *StateManager {
	return &StateManager{
		pending: make(map[string]*Status),
		running: make(map[string]*Status),
		done:    list.New(),
	}
}

func (m *StateManager) setStatusPending(id, name string, j Job) {
	m.mx.Lock()
	defer m.mx.Unlock()

	if _, ok := m.pending[id]; ok {
		panic("duplicate job ID detected")
	}

	m.pending[id] = &Status{
		ID:     id,
		Name:   name,
		Status: JobStatusPending,
		Job:    j,
	}
}

func (m *StateManager) setStatusRunning(id string) {
	m.mx.Lock()
	defer m.mx.Unlock()

	js := m.pending[id]
	js.StartedAt = time.Now()
	js.Status = JobStatusRunning
	delete(m.pending, id)
	m.running[id] = js

	log.Printf("job %s (%s): starting", id, js.Name)
}

func (m *StateManager) setStatusDone(id string, err error) {
	m.mx.Lock()
	defer m.mx.Unlock()

	js := m.running[id]
	delete(m.running, id)
	js.CompletedAt = time.Now()
	js.Status = JobStatusDone
	js.Err = err

	m.done.PushFront(js)
	for m.done.Len() > logsToKeep {
		m.done.Remove(m.done.Back())
	}

	statusMsg := "ok"
	if err != nil {
		statusMsg = fmt.Sprintf("ERROR: %v", err)
	}
	elapsed := js.CompletedAt.Sub(js.StartedAt)
	log.Printf("job %s (%s): completed in %s, %s", id, js.Name, elapsed, statusMsg)
}

// WithStatus tracks a job through its lifetime. The name is used for
// display purposes and needs not be unique.
func (m *StateManager) WithStatus(j Job, name string) Job {
	sj := &statusJob{
		Job: j,
		mgr: m,
		id:  j.ID(),
	}
	if sj.id == "" {
		panic("job with null ID")
	}
	m.setStatusPending(sj.id, name, sj)

	// Give this job its own ID, in case WithStatus calls are
	// nested.
	return WithID(sj)
}

func jobStatusMapToSlice(m map[string]*Status) []Status {
	out := make([]Status, 0, len(m))
	for _, js := range m {
		out = append(out, *js)
	}
	return out
}

func jobStatusListToSlice(l *list.List) []Status {
	out := make([]Status, 0, l.Len())
	for e := l.Front(); e != nil; e = e.Next() {
		js := e.Value.(*Status)
		out = append(out, *js)
	}
	return out
}

// GetStatus returns three lists of Status objects, representing
// respectively pending jobs (waiting to run), running jobs, and
// completed jobs (ordered by decreasing timestamp).
func (m *StateManager) GetStatus() ([]Status, []Status, []Status) {
	m.mx.Lock()
	defer m.mx.Unlock()

	return jobStatusMapToSlice(m.pending), jobStatusMapToSlice(m.running), jobStatusListToSlice(m.done)
}

type statusJob struct {
	Job
	mgr *StateManager
	id  string
}

func (j *statusJob) RunContext(ctx context.Context) error {
	j.mgr.setStatusRunning(j.id)
	err := j.Job.RunContext(ctx)
	j.mgr.setStatusDone(j.id, err)
	return err
}

// SyncGroup runs all the given jobs synchronously, aborting on the
// first error.
func SyncGroup(jobs []Job) Job {
	if len(jobs) == 1 {
		return jobs[0]
	}
	return JobFunc(func(ctx context.Context) error {
		for _, j := range jobs {
			if err := j.RunContext(ctx); err != nil {
				return err
			}
		}
		return nil
	})
}

// AsyncGroup runs all the given jobs asynchronously, and waits for
// all of them to terminate. It fails if any return an error.
func AsyncGroup(jobs []Job) Job {
	if len(jobs) == 1 {
		return jobs[0]
	}
	return JobFunc(func(ctx context.Context) error {
		errCh := make(chan error)
		defer close(errCh)
		for _, j := range jobs {
			go func(j Job) {
				errCh <- j.RunContext(ctx)
			}(j)
		}

		merr := new(util.MultiError)
		for i := 0; i < len(jobs); i++ {
			if err := <-errCh; err != nil {
				merr.Add(err)
			}
		}

		return merr.OrNil()
	})
}
