package jobs

import (
	"context"
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"sync"
	"sync/atomic"
	"testing"
	"time"
)

type testJobCounter struct {
	mx      sync.Mutex
	counter map[string]int
}

func newTestJobCounter() *testJobCounter {
	return &testJobCounter{
		counter: make(map[string]int),
	}
}

func (c *testJobCounter) incr(key string) {
	c.mx.Lock()
	c.counter[key]++
	c.mx.Unlock()
}

func (c *testJobCounter) withCounter(key string, job Job) Job {
	return JobFunc(func(ctx context.Context) error {
		c.incr(key)
		return job.RunContext(ctx)
	})
}

// JobGeneratorFunc that will generate a job that waits a little.
func waitJob(jc *testJobCounter, key string) JobGeneratorFunc {
	return func() Job {
		return jc.withCounter(key, JobFunc(func(ctx context.Context) error {
			c := time.After(300 * time.Millisecond)
			select {
			case <-c:
			case <-ctx.Done():
				return ctx.Err()
			}
			return nil
		}))
	}
}

func TestScheduler(t *testing.T) {
	tmpf, err := ioutil.TempFile("", "")
	if err != nil {
		t.Fatal(err)
	}
	tmpf.Close()
	defer os.Remove(tmpf.Name()) // nolint

	jc := newTestJobCounter()
	sched := NewSchedule(context.Background(), 1234)
	if err := sched.Add("job1", "@every 1s", waitJob(jc, "job1")); err != nil {
		t.Fatalf("Add(job1): %v", err)
	}
	if err := sched.Add("job2", "@random_every 3s", waitJob(jc, "job2")); err != nil {
		t.Fatalf("Add(job2): %v", err)
	}

	s := NewScheduler()
	s.SetSchedule(sched)
	time.Sleep(4 * time.Second)

	status := s.GetStatus()
	s.Stop()

	src1n := jc.counter["job1"]
	if src1n == 0 {
		t.Error("job1 count is zero")
	}
	if src1n > 10 {
		t.Errorf("job1 ran too many times (%d)", src1n)
	}
	src2n := jc.counter["job2"]
	if src2n == 0 {
		t.Error("job2 count is zero")
	}
	if src2n > 10 {
		t.Errorf("job2 ran too many times (%d)", src2n)
	}

	if len(status) != 2 {
		t.Errorf("bad status.Scheduled (expected 2 values): %+v", status)
	}
}

func TestRandomPeriodicSchedule(t *testing.T) {
	// To improve the statistical odds of finding something wrong,
	// run a large number of identical tests, with different
	// random seeds, in parallel.
	numTests := 1000

	var errcount int32
	var sampleErr error
	var wg sync.WaitGroup
	wg.Add(numTests)
	for i := 0; i < numTests; i++ {
		go func(i int) {
			if err := runRandomPeriodicScheduleTest(i); err != nil {
				if atomic.AddInt32(&errcount, 1) == 1 {
					sampleErr = err
				}
			}
			wg.Done()
		}(i)
	}

	wg.Wait()
	if errcount > 0 {
		t.Fatalf("found %d errors (on %d tests), sample: %v", errcount, numTests, sampleErr)
	}
}

func runRandomPeriodicScheduleTest(i int) error {
	rnd := rand.New(rand.NewSource(int64(i)))
	period := 30 * time.Second
	s := randomScheduleEvery(rnd, period)
	now := time.Now()
	next := s.Next(now)

	if next.Before(now) {
		return fmt.Errorf("next before now: next=%s, now=%s", next, now)
	}
	delta := next.Sub(now)
	if delta > period {
		return fmt.Errorf("delta greater than period: next=%s, now=%s, delta=%s", next, now, delta)
	}
	return nil
}
