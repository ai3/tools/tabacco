package tabacco

import (
	"encoding/binary"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"sync"

	"git.autistici.org/ai3/go-common/clientutil"
	"gopkg.in/yaml.v3"

	"git.autistici.org/ai3/tools/tabacco/jobs"
	"git.autistici.org/ai3/tools/tabacco/util"
)

var defaultSeedFile = "/var/tmp/.tabacco_scheduler_seed"

// Config is the global configuration object. While the actual
// configuration is spread over multiple files and directories, this
// holds it all together.
type Config struct {
	Hostname             string                    `yaml:"hostname"`
	Queue                *jobs.QueueSpec           `yaml:"queue_config"`
	Repository           RepositorySpec            `yaml:"repository"`
	DryRun               bool                      `yaml:"dry_run"`
	DefaultNiceLevel     int                       `yaml:"default_nice_level"`
	DefaultIOClass       int                       `yaml:"default_io_class"`
	WorkDir              string                    `yaml:"work_dir"`
	RandomSeedFile       string                    `yaml:"random_seed_file"`
	MetadataStoreBackend *clientutil.BackendConfig `yaml:"metadb"`

	HandlerSpecs []*HandlerSpec
	SourceSpecs  []*SourceSpec
}

// RuntimeContext provides access to runtime objects whose lifetime is
// ultimately tied to the configuration. Configuration can change
// during the lifetime of the process, but we want backup jobs to have
// a consistent view of the configuration while they execute, so
// access to the current version of the configuration is controlled to
// the ConfigManager.
type RuntimeContext interface {
	Shell() *Shell
	Repo() Repository
	QueueSpec() *jobs.QueueSpec
	Seed() int64
	WorkDir() string
	SourceSpecs() []*SourceSpec
	FindSource(string) *SourceSpec
	HandlerSpec(string) *HandlerSpec
	Close()
}

// The set of objects that are created from a Config and that the main
// code cares about.
type parsedConfig struct {
	handlerMap        map[string]*HandlerSpec
	sourceSpecs       []*SourceSpec
	sourceSpecsByName map[string]*SourceSpec
	queue             *jobs.QueueSpec

	repo    Repository
	seed    int64
	shell   *Shell
	workDir string
}

func (a *parsedConfig) Close() {
	a.repo.Close() // nolint
}

func (a *parsedConfig) Shell() *Shell              { return a.shell }
func (a *parsedConfig) Repo() Repository           { return a.repo }
func (a *parsedConfig) QueueSpec() *jobs.QueueSpec { return a.queue }
func (a *parsedConfig) Seed() int64                { return a.seed }
func (a *parsedConfig) WorkDir() string            { return a.workDir }
func (a *parsedConfig) SourceSpecs() []*SourceSpec { return a.sourceSpecs }

func (a *parsedConfig) HandlerSpec(name string) *HandlerSpec {
	return a.handlerMap[name]
}

func (a *parsedConfig) FindSource(name string) *SourceSpec {
	return a.sourceSpecsByName[name]
}

func buildHandlerMap(specs []*HandlerSpec) map[string]*HandlerSpec {
	// Create a handler map with a default 'file' spec.
	m := map[string]*HandlerSpec{
		"file": &HandlerSpec{
			Name: "file",
			Type: "file",
		},
	}
	for _, h := range specs {
		m[h.Name] = h
	}
	return m
}

func (c *Config) parse() (*parsedConfig, error) {
	shell := NewShell(c.DryRun)
	shell.SetNiceLevel(c.DefaultNiceLevel)
	shell.SetIOClass(c.DefaultIOClass)

	// Parse the repository config. An error here is fatal, as we
	// don't have a way to operate without a repository.
	repo, err := c.Repository.Parse()
	if err != nil {
		return nil, err
	}

	merr := new(util.MultiError)

	// Build the handlers.
	handlerMap := buildHandlerMap(c.HandlerSpecs)

	// Validate the sources (Parse is called later at runtime).
	// Sources that fail the check are removed from the
	// SourceSpecs array. We also check that sources have unique
	// names.
	srcMap := make(map[string]*SourceSpec)
	var srcs []*SourceSpec
	for _, spec := range c.SourceSpecs {
		if err := spec.Check(handlerMap); err != nil {
			merr.Add(fmt.Errorf("source %s: %v", spec.Name, err))
			continue
		}
		if _, ok := srcMap[spec.Name]; ok {
			merr.Add(fmt.Errorf("duplicated source %s", spec.Name))
			continue
		}
		srcMap[spec.Name] = spec
		srcs = append(srcs, spec)
	}

	// Read (or create) the seed file.
	seedFile := defaultSeedFile
	if c.RandomSeedFile != "" {
		seedFile = c.RandomSeedFile
	}
	seed := mustGetSeed(seedFile)

	return &parsedConfig{
		handlerMap:        handlerMap,
		sourceSpecs:       srcs,
		sourceSpecsByName: srcMap,
		queue:             c.Queue,
		shell:             shell,
		repo:              repo,
		seed:              seed,
		workDir:           c.WorkDir,
	}, merr.OrNil()
}

// The following functions read YAML files from .d-style directories. To be nice
// to the user, each file can contain either a single object or a list of
// multiple objects.
func readHandlersFromDir(dir string) ([]*HandlerSpec, error) {
	var out []*HandlerSpec
	err := foreachYAMLFile(dir, func(path string) error {
		var specs []*HandlerSpec
		log.Printf("reading handler: %s", path)
		if err := readYAMLFile(path, &specs); err != nil {
			var spec HandlerSpec
			if err := readYAMLFile(path, &spec); err != nil {
				return err
			}
			specs = append(specs, &spec)
		}
		out = append(out, specs...)
		return nil
	})
	return out, err
}

func readSourcesFromDir(dir string) ([]*SourceSpec, error) {
	var out []*SourceSpec
	err := foreachYAMLFile(dir, func(path string) error {
		var specs []*SourceSpec
		log.Printf("reading source: %s", path)
		if err := readYAMLFile(path, &specs); err != nil {
			var spec SourceSpec
			if err := readYAMLFile(path, &spec); err != nil {
				return err
			}
			specs = append(specs, &spec)
		}
		out = append(out, specs...)
		return nil
	})
	return out, err
}

// ReadConfig reads the configuration from the given path. Sources and
// handlers are read from the 'sources' and 'handlers' subdirectories
// of the directory containing the main configuration file.
//
// Performs a first level of static validation.
func ReadConfig(path string) (*Config, error) {
	// Read and validate the main configuration from 'path'.
	var config Config
	log.Printf("reading config: %s", path)
	if err := readYAMLFile(path, &config); err != nil {
		return nil, err
	}

	// Read handlers and sources from subdirectories of the
	// directory containing 'path'.
	dir := filepath.Dir(path)

	handlerSpecs, err := readHandlersFromDir(filepath.Join(dir, "handlers"))
	if err != nil {
		logMultiError("warning: handler configuration error: ", err)
		if len(handlerSpecs) == 0 {
			return nil, errors.New("no configured handlers")
		}
	}
	config.HandlerSpecs = handlerSpecs

	sourceSpecs, err := readSourcesFromDir(filepath.Join(dir, "sources"))
	if err != nil {
		logMultiError("warning: source configuration error: ", err)
		if len(sourceSpecs) == 0 {
			return nil, errors.New("no configured sources")
		}
	}
	config.SourceSpecs = sourceSpecs

	return &config, nil
}

func logMultiError(prefix string, err error) {
	if merr, ok := err.(*util.MultiError); ok {
		for _, e := range merr.Errors() {
			log.Printf("%s%v", prefix, e)
		}
	} else {
		log.Printf("%s%v", prefix, err)
	}
}

func readYAMLFile(path string, obj interface{}) error {
	f, err := os.Open(path)
	if err != nil {
		return err
	}
	defer f.Close()
	return yaml.NewDecoder(f).Decode(obj)
}

func foreachYAMLFile(dir string, f func(string) error) error {
	files, err := filepath.Glob(filepath.Join(dir, "*.yml"))
	if err != nil {
		return err
	}
	merr := new(util.MultiError)
	for _, path := range files {
		if err := f(path); err != nil {
			log.Printf("error loading yaml file %s: %v", path, err)
			merr.Add(err)
		}
	}
	return merr.OrNil()
}

// ConfigManager holds all runtime data derived from the configuration
// itself, so it can be easily reloaded by calling Reload(). Listeners
// should register themselves with Notify() in order to be updated
// when the configuration changes (there is currently no way to
// unregister).
type ConfigManager struct {
	mx     sync.Mutex
	parsed *parsedConfig

	// Listeners are notified on every reload.
	notifyCh  chan struct{}
	listeners []chan struct{}
}

// NewConfigManager creates a new ConfigManager.
func NewConfigManager(config *Config) (*ConfigManager, error) {
	m := &ConfigManager{
		notifyCh: make(chan struct{}, 1),
	}
	if err := m.Reload(config); err != nil {
		return nil, err
	}
	go func() {
		for range m.notifyCh {
			m.mx.Lock()
			for _, lch := range m.listeners {
				select {
				case lch <- struct{}{}:
				default:
				}
			}
			m.mx.Unlock()
		}
	}()
	return m, nil
}

// Reload the configuration (at least, the parts of it that can be
// dynamically reloaded).
func (m *ConfigManager) Reload(config *Config) error {
	parsed, err := config.parse()
	if parsed == nil {
		return err
	} else if err != nil {
		log.Printf("warning: errors in configuration: %v", err)
	}

	// Update config and notify listeners (in a separate
	// goroutine, that does not hold the lock).
	m.mx.Lock()
	defer m.mx.Unlock()
	if m.parsed != nil {
		m.parsed.Close() // nolint
	}

	log.Printf("loaded new config: %d handlers, %d sources", len(parsed.handlerMap), len(parsed.sourceSpecs))
	m.parsed = parsed
	m.notifyCh <- struct{}{}
	return nil
}

// Close the ConfigManager and all associated resources.
func (m *ConfigManager) Close() {
	m.mx.Lock()
	close(m.notifyCh)
	if m.parsed != nil {
		m.parsed.Close()
	}
	m.mx.Unlock()
}

// Notify the caller when the configuration is reloaded.
func (m *ConfigManager) Notify() <-chan struct{} {
	m.mx.Lock()
	defer m.mx.Unlock()

	// Create the channel and prime it with a value so the
	// listener loads its initial configuration.
	ch := make(chan struct{}, 1)
	ch <- struct{}{}
	m.listeners = append(m.listeners, ch)
	return ch
}

// NewRuntimeContext returns a new RuntimeContext, capturing current
// configuration and runtime assets.
func (m *ConfigManager) NewRuntimeContext() RuntimeContext {
	return m.current()
}

func (m *ConfigManager) current() *parsedConfig {
	m.mx.Lock()
	defer m.mx.Unlock()
	return m.parsed
}

func mustGetSeed(path string) int64 {
	if data, err := ioutil.ReadFile(path); err == nil && len(data) == 8 { // nolint: gosec
		if seed := binary.LittleEndian.Uint64(data); seed > 0 {
			return int64(seed)
		}
	}
	log.Printf("generating new random seed for this host")
	seed, data := util.RandomSeed()
	if err := ioutil.WriteFile(path, data, 0600); err != nil {
		log.Printf("warning: can't write random seed file: %v", err)
	}
	return seed
}
