package tabacco

import (
	"context"
	"log"
	"os"

	"git.autistici.org/ai3/tools/tabacco/jobs"
)

type logContextKeyType int

var logContextKey = 0

var defaultLogger = log.New(os.Stdout, "", 0)

func WithLogPrefix(ctx context.Context, pfx string) context.Context {
	// Fetch logger from current prefix.
	root := GetLogger(ctx)
	l := log.New(root.Writer(), pfx+": ", 0)
	return context.WithValue(ctx, &logContextKey, l)
}

func GetLogger(ctx context.Context) *log.Logger {
	l, ok := ctx.Value(&logContextKey).(*log.Logger)
	if !ok {
		return defaultLogger
	}
	return l
}

func JobWithLogPrefix(j jobs.Job, pfx string) jobs.Job {
	return jobs.JobFunc(func(ctx context.Context) error {
		lctx := WithLogPrefix(ctx, pfx)
		return j.RunContext(lctx)
	})
}
