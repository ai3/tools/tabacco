package tabacco

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"strings"
	"sync"
	"time"

	"git.autistici.org/ai3/tools/tabacco/jobs"
	"github.com/hashicorp/go-version"
)

// We need to check that we're running at least restic 0.9, or the
// restore functionality won't work as expected.
var (
	resticVersionRx = regexp.MustCompile(`^restic ([0-9.]+)`)

	resticMinGoodVersion        = version.Must(version.NewVersion("0.9"))
	resticRepositoryFileVersion = version.Must(version.NewVersion("0.11"))
	resticFixedIssue3104Version = version.Must(version.NewVersion("0.12"))
)

func getResticVersion(bin string) (*version.Version, error) {
	output, err := exec.Command(bin, "version").Output() // #nosec
	if err != nil {
		return nil, err
	}
	m := resticVersionRx.FindStringSubmatch(string(output))
	if len(m) < 2 {
		return nil, errors.New("could not parse restic version")
	}
	return version.NewVersion(m[1])
}

type resticRepository struct {
	bin            string
	version        *version.Version
	uri            string
	tmpDir         string
	passwordFile   string
	repositoryFile string
	excludes       []string
	excludeFiles   []string
	autoPrune      bool
	cacheMgr       cacheManager

	initialized sync.Once
}

func (r *resticRepository) resticCmd(cmd string) string {
	args := []string{r.bin, cmd}

	// Work around https://github.com/restic/restic/issues/3104
	// ("restic init" does not respect --repository-file in Restic
	// versions prior to 0.12).
	if r.version.LessThan(resticRepositoryFileVersion) || (cmd == "init" && r.version.LessThan(resticFixedIssue3104Version)) {
		args = append(args, "--repo", r.uri)
	} else {
		args = append(args, "--repository-file", r.repositoryFile)
	}

	args = append(args, "--password-file", r.passwordFile)

	return strings.Join(args, " ")
}

func (r *resticRepository) excludeArgs() string {
	var args []string
	for _, x := range r.excludes {
		args = append(args, "--exclude", x)
	}
	for _, x := range r.excludeFiles {
		args = append(args, "--exclude-file", x)
	}
	return strings.Join(args, " ")
}

// newResticRepository returns a restic repository.
func newResticRepository(params Params) (Repository, error) {
	uri := params.Get("uri")
	if uri == "" {
		return nil, errors.New("missing uri")
	}
	password := params.Get("password")
	if password == "" {
		return nil, errors.New("missing password")
	}

	bin := "restic"
	if s := params.Get("restic_binary"); s != "" {
		bin = s
	}
	resticVersion, err := getResticVersion(bin)
	if err != nil {
		return nil, err
	}
	if resticVersion.LessThan(resticMinGoodVersion) {
		return nil, fmt.Errorf(
			"restic version %s is older than the minimum supported version %s",
			resticVersion, resticMinGoodVersion)
	}
	log.Printf("detected restic version %s", resticVersion)

	// Create a temporary directory and write repository URL and
	// password in files therein.
	tmpDir, err := ioutil.TempDir("", "tabacco-restic-")
	if err != nil {
		return nil, err
	}
	passwordFile := filepath.Join(tmpDir, "pw")
	if err := ioutil.WriteFile(passwordFile, []byte(password), 0600); err != nil {
		return nil, err
	}
	repositoryFile := filepath.Join(tmpDir, "repo")
	if err := ioutil.WriteFile(repositoryFile, []byte(uri), 0600); err != nil {
		return nil, err
	}

	var cmgr cacheManager = new(nullCacheManager)
	if s := params.Get("cache_dir"); s != "" {
		cmgr = newResticCacheManager(s)
	}

	autoPrune := true
	if b, ok := params.GetBool("autoprune"); ok {
		autoPrune = b
	}

	return &resticRepository{
		bin:            bin,
		version:        resticVersion,
		uri:            uri,
		tmpDir:         tmpDir,
		passwordFile:   passwordFile,
		repositoryFile: repositoryFile,
		excludes:       params.GetList("exclude"),
		excludeFiles:   params.GetList("exclude_files"),
		autoPrune:      autoPrune,
		cacheMgr:       cmgr,
	}, nil
}

func (r *resticRepository) Close() error {
	return os.RemoveAll(r.tmpDir)
}

func (r *resticRepository) Init(ctx context.Context, rctx RuntimeContext) error {
	r.initialized.Do(func() {
		// Restic init will fail if the repository is already
		// initialized, ignore errors (but log them).
		if err := rctx.Shell().Run(ctx, fmt.Sprintf(
			"%s --quiet || true",
			r.resticCmd("init"),
		)); err != nil {
			GetLogger(ctx).Printf("restic repository init failed (likely harmless): %v", err)
		}
	})
	return nil
}

func (r *resticRepository) Prepare(ctx context.Context, rctx RuntimeContext, backup *Backup) error {
	if !r.autoPrune {
		return nil
	}
	return rctx.Shell().Run(ctx, fmt.Sprintf(
		"%s --host %s --keep-last 10 --prune",
		r.resticCmd("forget"),
		backup.Host,
	))
}

func resticBackupTags(backup *Backup, ds *Dataset) string {
	return fmt.Sprintf("--tag dataset_id=%s --tag backup_id=%s --tag dataset_source=%s", ds.ID, backup.ID, ds.Source)
}

func (r *resticRepository) backupCmd(cc cache, backup *Backup, ds *Dataset, inputFile string, exclude []string) string {
	cmd := fmt.Sprintf(
		"%s %s %s --json --exclude-caches --one-file-system %s --files-from %s",
		r.resticCmd("backup"),
		r.excludeArgs(),
		cc.Args(),
		resticBackupTags(backup, ds),
		inputFile,
	)
	for _, ex := range exclude {
		cmd += fmt.Sprintf(" --exclude='%s'", ex)
	}
	return cmd
}

func (r *resticRepository) getSnapshotID(ctx context.Context, cc cache, shell *Shell, backup *Backup, ds *Dataset) (string, error) {
	if ds.SnapshotID != "" {
		return ds.SnapshotID, nil
	}

	// Legacy compatibility: query restic using the dataset ID.
	data, err := shell.Output(ctx, fmt.Sprintf(
		"%s %s --no-lock --json %s",
		r.resticCmd("snapshots"),
		cc.Args(),
		resticBackupTags(backup, ds),
	))
	if err != nil {
		return "", err
	}
	snaps, err := parseResticSnapshots(data)
	if err != nil {
		return "", err
	}
	if len(snaps) < 1 {
		return "", fmt.Errorf("could not find snapshot for backup id %s", backup.ID)
	}
	return snaps[0].ShortID, nil
}

func (r *resticRepository) restoreCmd(ctx context.Context, cc cache, shell *Shell, backup *Backup, ds *Dataset, paths []string, target string) (string, error) {
	snap, err := r.getSnapshotID(ctx, cc, shell, backup, ds)
	if err != nil {
		return "", err
	}

	cmd := []string{
		r.resticCmd("restore"),
	}

	for _, path := range paths {
		cmd = append(cmd, fmt.Sprintf("--include %s", path))
	}

	cmd = append(cmd, fmt.Sprintf("--target %s", target))
	cmd = append(cmd, cc.Args())
	cmd = append(cmd, snap)
	return strings.Join(cmd, " "), nil
}

// A special path for stdin datasets that is likely to be unused by the
// rest of the filesystem (the path namespace in Restic is global).
func datasetStdinPath(ds *Dataset) string {
	var dsPath string
	switch len(ds.Atoms) {
	case 0:
		dsPath = ds.Source
	case 1:
		dsPath = filepath.Join(ds.Source, ds.Atoms[0].Name)
	default:
		dsPath = filepath.Join(ds.Source, "all")
	}
	dsPath = strings.TrimPrefix(dsPath, "/")
	dsPath = strings.Replace(dsPath, "/", "_", -1)
	return fmt.Sprintf("/STDIN_%s", dsPath)
}

func (r *resticRepository) backupStreamCmd(cc cache, backup *Backup, ds *Dataset) string {
	fakePath := datasetStdinPath(ds)
	return fmt.Sprintf(
		// The --force prevents restic from trying to find a previous snapshot.
		"%s %s %s --json --force --stdin --stdin-filename %s",
		r.resticCmd("backup"),
		cc.Args(),
		resticBackupTags(backup, ds),
		fakePath,
	)
}

func (r *resticRepository) restoreStreamCmd(ctx context.Context, cc cache, shell *Shell, backup *Backup, ds *Dataset, target string) (string, error) {
	snap, err := r.getSnapshotID(ctx, cc, shell, backup, ds)
	if err != nil {
		return "", err
	}

	fakePath := datasetStdinPath(ds)
	targetPath := filepath.Base(fakePath)

	// Restore the file to a temporary directory, then pipe it.
	return fmt.Sprintf(
		"(%s %s --target %s %s 1>&2 && cat %s)",
		r.resticCmd("restore"),
		cc.Args(),
		target,
		snap,
		filepath.Join(target, targetPath),
	), nil
}

func withCache(mgr cacheManager, fn func(cache) error) error {
	cc, err := mgr.Acquire()
	if err != nil {
		return err
	}
	defer mgr.Release(cc)

	return fn(cc)
}

func (r *resticRepository) RunBackup(ctx context.Context, shell *Shell, backup *Backup, ds *Dataset, inputFile string, exclude []string) error {
	return withCache(r.cacheMgr, func(cc cache) error {
		cmd := r.backupCmd(cc, backup, ds, inputFile, exclude)
		return r.runBackupCmd(ctx, shell, backup, ds, cmd)
	})
}

func (r *resticRepository) RunStreamBackup(ctx context.Context, shell *Shell, backup *Backup, ds *Dataset, backupCmd, compressCmd string) error {
	return withCache(r.cacheMgr, func(cc cache) error {
		// Concatenate the backupCmd with restic (and an optional
		// compression command) to build a shell pipeline.
		pipe := []string{
			fmt.Sprintf("(%s)", expandVars(backupCmd, backup, ds)),
		}
		if compressCmd != "" {
			pipe = append(pipe, compressCmd)
		}
		pipe = append(pipe, r.backupStreamCmd(cc, backup, ds))

		cmd := strings.Join(pipe, " | ")
		return r.runBackupCmd(ctx, shell, backup, ds, cmd)
	})
}

func (r *resticRepository) RunRestore(ctx context.Context, shell *Shell, backup *Backup, ds *Dataset, paths []string, target string) error {
	return withCache(r.cacheMgr, func(cc cache) error {
		cmd, err := r.restoreCmd(ctx, cc, shell, backup, ds, paths, target)
		if err != nil {
			return err
		}
		return shell.Run(ctx, cmd)
	})
}

func (r *resticRepository) RunStreamRestore(ctx context.Context, shell *Shell, backup *Backup, ds *Dataset, restoreCmd, decompressCmd string) error {
	return withCache(r.cacheMgr, func(cc cache) error {
		resticCmd, err := r.restoreStreamCmd(ctx, cc, shell, backup, ds, getWorkDir(ctx))
		if err != nil {
			return err
		}

		pipe := []string{resticCmd}
		if decompressCmd != "" {
			pipe = append(pipe, decompressCmd)
		}
		pipe = append(pipe, fmt.Sprintf("(%s)", expandVars(restoreCmd, backup, ds)))
		cmd := strings.Join(pipe, " | ")
		return shell.Run(ctx, cmd)
	})
}

// Global map that keeps track of the currently running restic
// processes and their respective progress, for debugging purposes.
type activeBackupStatus struct {
	sync.Mutex

	Backup  *Backup
	Dataset *Dataset
	Status  *resticStatusMessage
}

type activeBackups struct {
	mx     sync.Mutex
	active map[string]*activeBackupStatus
}

var active *activeBackups

func init() {
	active = &activeBackups{
		active: make(map[string]*activeBackupStatus),
	}
}

func (a *activeBackups) WithResticStatus(jobID string, backup *Backup, ds *Dataset, f func(chan *resticStatusMessage) error) error {
	ch := make(chan *resticStatusMessage, 1)

	status := &activeBackupStatus{
		Backup:  backup,
		Dataset: ds,
	}
	a.mx.Lock()
	a.active[jobID] = status
	a.mx.Unlock()

	go func() {
		for resticStatus := range ch {
			status.Lock()
			status.Status = resticStatus
			status.Unlock()
		}
	}()

	err := f(ch)

	close(ch)

	a.mx.Lock()
	delete(a.active, jobID)
	a.mx.Unlock()

	return err
}

func (a *activeBackups) GetJobStatus() []*JobStatus {
	a.mx.Lock()
	defer a.mx.Unlock()

	out := make([]*JobStatus, 0, len(a.active))
	for jobID, status := range a.active {
		status.Lock()
		var resticStatus *RunningJobStatus
		if status.Status != nil {
			resticStatus = new(RunningJobStatus)
			*resticStatus = RunningJobStatus(*status.Status)
		}
		js := &JobStatus{
			JobID:         jobID,
			Host:          status.Backup.Host,
			BackupID:      status.Backup.ID,
			DatasetID:     status.Dataset.ID,
			DatasetSource: status.Dataset.Source,
			Status:        resticStatus,
		}
		out = append(out, js)
		status.Unlock()
	}
	return out
}

// JSON status message.
type resticStatusMessage struct {
	SecondsElapsed int64    `json:"seconds_elapsed"`
	PercentDone    float64  `json:"percent_done"`
	TotalFiles     int64    `json:"total_files"`
	FilesDone      int64    `json:"files_done"`
	TotalBytes     int64    `json:"total_bytes"`
	BytesDone      int64    `json:"bytes_done"`
	CurrentFiles   []string `json:"current_files,omitempty"`
}

// JSON summary message.
type resticSummaryMessage struct {
	FilesNew            int64   `json:"files_new"`
	FilesChanged        int64   `json:"files_changed"`
	FilesUnmodified     int64   `json:"files_unmodified"`
	DirsNew             int64   `json:"dirs_new"`
	DirsChanged         int64   `json:"dirs_changed"`
	DirsUnmodified      int64   `json:"dirs_unmodified"`
	DataBlobs           int64   `json:"data_blobs"`
	TreeBlobs           int64   `json:"tree_blobs"`
	DataAdded           int64   `json:"data_added"`
	TotalFilesProcessed int64   `json:"total_files_processed"`
	TotalBytesProcessed int64   `json:"total_bytes_processed"`
	TotalDuration       float64 `json:"total_duration"`
	SnapshotID          string  `json:"snapshot_id"`
}

// Generic JSON message from 'restic backup'.
type resticMessage struct {
	MessageType string `json:"message_type"`
	resticStatusMessage
	resticSummaryMessage
}

// Run a backup command. The output of 'restic backup' is examined for
// progress and to obtain the snapshot ID. Modifies the provided
// backup/dataset objects.
func (r *resticRepository) runBackupCmd(ctx context.Context, shell *Shell, backup *Backup, ds *Dataset, cmd string) error {
	return active.WithResticStatus(jobs.GetID(ctx), backup, ds, func(progressCh chan *resticStatusMessage) error {
		err := shell.RunWithStdoutCallback(ctx, cmd, func(line []byte) {
			var msg resticMessage
			if err := json.Unmarshal(line, &msg); err != nil {
				GetLogger(ctx).Printf("error parsing restic JSON message: %v (%s)", err, line)
				return
			}
			switch msg.MessageType {
			case "status":
				progressCh <- &msg.resticStatusMessage
			case "summary":
				GetLogger(ctx).Printf("dataset %s: total_bytes=%d, new=%d", ds.ID, msg.TotalBytesProcessed, msg.DataAdded)
				ds.SnapshotID = msg.SnapshotID
				ds.TotalFiles = msg.TotalFilesProcessed
				ds.TotalBytes = msg.TotalBytesProcessed
				ds.BytesAdded = msg.DataAdded
				ds.Duration = int(msg.TotalDuration)
			}
		})
		var exitErr *exec.ExitError
		if errors.As(err, &exitErr) && exitErr.ExitCode() == 3 {
			// We can safely ignore exit status 3 from restic.
			err = nil
		}
		return err
	})
}

// Data about a snapshot, obtained from 'restic snapshots --json'.
type resticSnapshot struct {
	ID       string    `json:"id"`
	ShortID  string    `json:"short_id"`
	Time     time.Time `json:"time"`
	Parent   string    `json:"parent"`
	Tree     string    `json:"tree"`
	Hostname string    `json:"hostname"`
	Username string    `json:"username"`
	UID      int       `json:"uid"`
	GID      int       `json:"gid"`
	Paths    []string  `json:"paths"`
	Tags     []string  `json:"tags"`
}

func parseResticSnapshots(output []byte) (snapshots []resticSnapshot, err error) {
	// An empty input is not necessarily an error.
	if len(output) > 0 {
		err = json.Unmarshal(output, &snapshots)
	}
	return
}
