package tabacco

import (
	"context"
	"testing"

	"git.autistici.org/ai3/tools/tabacco/jobs"
)

type fakeManager struct{}

func (m *fakeManager) BackupJob(context.Context, *SourceSpec) (*Backup, jobs.Job, error) {
	return &Backup{}, nil, nil
}

func (m *fakeManager) Backup(context.Context, *SourceSpec) (*Backup, error) {
	return &Backup{}, nil
}

func (m *fakeManager) RestoreJob(context.Context, *FindRequest, string) (jobs.Job, error) {
	return nil, nil
}

func (m *fakeManager) Restore(context.Context, *FindRequest, string) error {
	return nil
}

func (m *fakeManager) Close() error {
	return nil
}

func (m *fakeManager) GetStatus() ([]jobs.Status, []jobs.Status, []jobs.Status) {
	return nil, nil, nil
}

func TestMakeSchedule(t *testing.T) {
	sourceSpecs := []*SourceSpec{
		&SourceSpec{
			Name:     "source1/users",
			Handler:  "file1",
			Schedule: "@random_every 1d",
			Datasets: []*DatasetSpec{
				&DatasetSpec{
					Atoms: []Atom{
						{
							Name: "user1",
							Path: "user1",
						},
						{
							Name: "user2",
							Path: "user2",
						},
					},
				},
			},
		},
		&SourceSpec{
			Name:            "source2",
			Handler:         "dbpipe",
			Schedule:        "35 3 * * *",
			DatasetsCommand: "echo user1 user1 ; echo user2 user2",
		},
	}

	mgr := &fakeManager{}
	_, err := makeSchedule(context.Background(), mgr, sourceSpecs, 1234)
	if err != nil {
		t.Fatal(err)
	}
}
