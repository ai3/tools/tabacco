package tabacco

import (
	"errors"
	"fmt"
	"path/filepath"
	"sync"
)

const maxConcurrentCaches = 20

type cacheManager interface {
	Acquire() (cache, error)
	Release(cache)
}

type cache interface {
	Args() string
}

// The restic cache manager maintains a pool of cache
// directories. These can take quite a lot of disk space, so we only
// create more than one when jobs are running in parallel.
type resticCacheManager struct {
	root string

	mx   sync.Mutex
	busy []bool
}

func newResticCacheManager(root string) *resticCacheManager {
	return &resticCacheManager{
		root: root,
		busy: make([]bool, maxConcurrentCaches),
	}
}

func (m *resticCacheManager) next() (int, bool) {
	for i := 0; i < len(m.busy); i++ {
		if !m.busy[i] {
			m.busy[i] = true
			return i, true
		}
	}
	return 0, false
}

func (m *resticCacheManager) Acquire() (cache, error) {
	m.mx.Lock()
	defer m.mx.Unlock()

	index, ok := m.next()
	if !ok {
		return nil, errors.New("no more caches available")
	}
	return &resticCache{
		index: index,
		path:  filepath.Join(m.root, fmt.Sprintf("cache-%03d", index)),
	}, nil
}

func (m *resticCacheManager) Release(c cache) {
	m.mx.Lock()
	rc := c.(*resticCache)
	m.busy[rc.index] = false
	m.mx.Unlock()
}

type resticCache struct {
	index int
	path  string
}

func (c *resticCache) Args() string {
	return fmt.Sprintf("--cache-dir %s", c.path)
}

// The null cache manager always returns --no-cache.
type nullCacheManager struct{}

func (m *nullCacheManager) Acquire() (cache, error) {
	return new(nullCache), nil
}

func (m *nullCacheManager) Release(cache) {}

type nullCache struct{}

func (c *nullCache) Args() string {
	return "--no-cache"
}
