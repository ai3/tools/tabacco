FROM golang:1.23.2 as build
ADD . /src
RUN cd /src && \
    go build -ldflags="-extldflags=-static" -tags sqlite_omit_load_extension,netgo,osusergo -o tabacco ./cmd/tabacco && \
    strip tabacco

FROM scratch
COPY --from=build /src/tabacco /tabacco
ENTRYPOINT ["/tabacco", "metadb"]

